<?php

namespace Modules\Blog\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comments extends Model
{
	use SoftDeletes;
	
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'comments';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['*'];
    
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Get post that owns the comment.
     */
    public function post()
    {
        return $this->belongsTo('Modules\Blog\Entities\Posts');
    }

    /**
     * Get user that owns the comment.
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    /**
     * Get comment children.
     */
    public function children()
    {
        return $this->hasMany('Modules\Blog\Entities\Comments', 'parent', 'id');
    }

    /**
     * Get comment parent.
     */
    public function commentParent()
    {
        return $this->belongsTo('Modules\Blog\Entities\Comments', 'parent');
    }
}
