<?php

namespace Modules\Blog\Entities;

use Illuminate\Database\Eloquent\Model;

class PostTags extends Model
{
	/**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'post_tags';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['post_id', 'tag_id'];

    /**
     * Get the post that owns the tag.
     */
    public function post()
    {
        return $this->belongsTo('Modules\Blog\Entities\Posts');
    }
}
