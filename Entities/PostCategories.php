<?php

namespace Modules\Blog\Entities;

use Illuminate\Database\Eloquent\Model;

class PostCategories extends Model
{
	/**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'post_categories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['post_id', 'category_id'];

    /**
     * Get the post that owns the category.
     */
    public function post()
    {
        return $this->belongsTo('Modules\Blog\Entities\Posts');
    }
}
