<?php

namespace Modules\Blog\Entities;

use Illuminate\Database\Eloquent\Model;

class media extends Model
{
	/**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'media';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['*'];
}
