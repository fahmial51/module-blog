<?php

namespace Modules\Blog\Entities;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class Posts extends Model
{
	use SoftDeletes;
	
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'posts';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['*'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Scope a query to only include posts of a given type.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param mixed $type
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePostType($query, $type)
    {
        return $query->where('post_type', $type);
    }

    /**
     * Scope a query to only include post of a given status publish and already published.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param mixed $type
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePublished($query)
    {
        return $query->where('status', 1)
                     ->where('published_at', '<=', Carbon::now());
    }

    /**
     * Get the user that owns the post.
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'author', 'id');
    }

    /**
     * Get the comments for the blog post.
     */
    public function comments()
    {
        return $this->hasMany('Modules\Blog\Entities\Comments', 'post_id');
    }

    /**
     * Get the metas for the blog post.
     */
    public function metas()
    {
        return $this->hasMany('Modules\Blog\Entities\PostMeta', 'post_id');
    }

    /**
     * the categories that belong to the post.
     */
    public function tags()
    {
        return $this->belongsToMany('Modules\Blog\Entities\Tags', 'post_tags', 'post_id', 'tag_id');
    }

    /**
     * the categories that belong to the post.
     */
    public function categories()
    {
        return $this->belongsToMany('Modules\Blog\Entities\Categories', 'post_categories', 'post_id', 'category_id');
    }

    /**
     * Get full slug post.
     */
    public function getFullSlugAttribute($value)
    {
        return $this->slug.'-'.$this->slug_id;
    }

    /**
     * Get post meta_title.
     */
    public function getMetaTitleAttribute($value)
    {
        $meta_title = $this->metas->where('meta_key', 'meta_title')->first()->meta_value ?? $this->title;
        $meta_title = ucwords($meta_title);
        return $meta_title;
    }

    /**
     * Get post meta_desc.
     */
    public function getMetaDescAttribute($value)
    {
        return $this->metas->where('meta_key', 'meta_desc')->first()->meta_value ?? str_limit(html_entity_decode(strip_tags($this->content)), 250);
    }

    /**
     * Get post meta_keyword.
     */
    public function getMetaKeywordAttribute($value)
    {
        return $this->metas->where('meta_key', 'meta_keyword')->first()->meta_value ?? '';
    }
}
