<?php

namespace Modules\Blog\Entities;

use Illuminate\Database\Eloquent\Model;

class PostMeta extends Model
{
	/**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'post_meta';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['post_id', 'meta_key', 'meta_value'];

    /**
     * Get the post that owns the metas.
     */
    public function post()
    {
        return $this->belongsTo('Modules\Blog\Entities\Posts');
    }
}
