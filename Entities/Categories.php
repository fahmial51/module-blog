<?php

namespace Modules\Blog\Entities;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
	/**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'categories';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['*'];

    /**
     * The post that belong to the tag.
     */
    public function posts()
    {
        return $this->belongsToMany('Modules\Blog\Entities\Posts', 'post_categories', 'category_id', 'post_id');
    }
}
