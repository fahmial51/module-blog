<?php

namespace Modules\Blog\Entities;

use Illuminate\Database\Eloquent\Model;

class Tags extends Model
{
	/**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tags';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['*'];

    /**
     * The post that belong to the tag.
     */
    public function posts()
    {
        return $this->belongsToMany('Modules\Blog\Entities\Posts', 'post_tags', 'tag_id', 'post_id');
    }
}
