<?php

namespace Modules\Blog\Entities;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
	/**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'options';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['meta_key', 'meta_value'];
}
