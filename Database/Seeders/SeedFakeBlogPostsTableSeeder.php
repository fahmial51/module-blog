<?php

namespace Modules\Blog\Database\Seeders;


use Faker\Factory as Faker;
use Modules\Blog\Http\Helpers\PostHelper;
use DB;

class SeedFakeBlogPostsTableSeeder extends BlogDatabaseSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $cat_id = DB::table('categories')->insertGetId([
                    'name' => 'category 1',
                    'slug' => 'category-1',
                    'description' => null,
                    'parent' => null,
                    'created_at' => date("Y-m-d H:i:s"),
                    'updated_at' => date("Y-m-d H:i:s")
                ]);
        $tag_id = DB::table('tags')->insertGetId([
                    'name' => 'tag 1',
                    'slug' => 'tag-1',
                    'description' => null,
                    'created_at' => date("Y-m-d H:i:s"),
                    'updated_at' => date("Y-m-d H:i:s")
                ]);
        foreach (range(1,10) as $index) {
            $word = $faker->word.' '.$faker->word;
            $id =  DB::table('posts')->insertGetId([
                    'slug_id' => substr($faker->word, 0, 4),
                    'slug' => str_slug($word),
                    'title' => $word,
                    'content' => '<p>'.$faker->paragraph.'</p>',
                    'featured_image' => asset('/img/img/tes.jpg');
                    'post_type' => 'post',
                    'status' => 'publish',
                    'author' => 1,
                    'published_at' => date("Y-m-d H:i:s"),
                    'created_at' => date("Y-m-d H:i:s"),
                    'updated_at' => date("Y-m-d H:i:s"),
                    'deleted_at' => null
                ]);

            if ($id) {
                
                DB::table('post_categories')->insert([
                    'post_id' => $id,
                    'category_id' => $cat_id,
                    'created_at' => date("Y-m-d H:i:s"),
                    'updated_at' => date("Y-m-d H:i:s")
                ]);
                DB::table('post_tags')->insert([
                    'post_id' => $id,
                    'tag_id' => $tag_id,
                    'created_at' => date("Y-m-d H:i:s"),
                    'updated_at' => date("Y-m-d H:i:s")
                ]);
                DB::table('comments')->insert([
                    'post_id' => $id,
                    'user_id' => 1,
                    'parent' => null,
                    'content' => $faker->paragraph,
                    'status' => 'publish',
                    'created_at' => date("Y-m-d H:i:s"),
                    'updated_at' => date("Y-m-d H:i:s"),
                    'deleted_at' => null
                ]);

                $meta[] = ['name' => 'meta_keyword', 'value' => $word];
                $meta[] = ['name' => 'meta_title', 'value' => $word];
                $meta[] = ['name' => 'meta_desc', 'value' => $word];

                foreach ($meta as $key => $value) {
                    DB::table('post_meta')->insert([
                        'post_id' => $id,
                        'meta_key' => $value['name'],
                        'meta_value' => $value['value']
                    ]);
                }
            }
        }
    }
}
