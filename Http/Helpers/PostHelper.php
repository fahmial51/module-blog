<?php
namespace Modules\Blog\Http\Helpers;

use Illuminate\Support\Facades\Storage;
use Modules\Blog\Entities\Posts;
use Modules\Blog\Entities\PostMeta;
use Modules\Blog\Entities\Categories;
use Modules\Blog\Entities\PostCategories;
use Modules\Blog\Entities\Tags;
use Modules\Blog\Entities\PostTags;
use Modules\Blog\Entities\Keywords;
use Illuminate\Http\File;
use App\Helpers\SSOHelper;
use Image;
use DB;

class PostHelper
{
    private $prefix;
    static $img_large;
    static $img_medium;
    static $img_thumb;

    public function __construct(){
        $this->prefix = 'admin/blog/';
        self::$img_large = 1200;
        self::$img_medium = 800;
        self::$img_thumb = 300;
    }

    /**
     * Get Page Templates List
     * @return Response
     */
    public static function get_page_templates_list(){
        $dir = resource_path('views/template');
        if (!file_exists($dir)) {
            return [];
        }
        $files = scandir($dir, 1);
        $templates = [];
        foreach ($files as $key => $file) {
            if (strpos($file, '.blade.php') !== false) {
                $templates[$key] = new \stdClass;;
                $templates[$key]->file_name = 'template.'.str_replace('.blade.php', '', $file);
                $templates[$key]->name = str_replace('.blade.php', ' page', $file);
            }
        }
        return $templates;
    }
    
    /**
     * Make slug.
     * @param  $string
     * @return Response
     */
    public static function make_slug($string){
        $slug_title = str_replace('(', '', $string);
        $slug_title = str_replace(')', '', $slug_title);
        $slug_title = str_replace("'", '', $slug_title);
        $slug_title = str_replace(":", '', $slug_title);
        $slug_title = str_replace(".", ' ', $slug_title);
        $slug_title = str_replace("&", '', $slug_title);
        $slug_title = str_replace(",", '', $slug_title);
        $slug_title = str_replace("`", '', $slug_title);
        $slug_title = str_replace("?", '', $slug_title);
        $slug_title = str_replace('"', '', $slug_title);
        $the_slug = str_replace(' ', '-', $slug_title);   
        $the_slug = strtolower($the_slug);
        return $the_slug;
    }

    /**
     * Get all category parent to select category parent.
     * @param  $category_id
     * @return Response
     */
    public static function get_category_parent($category_id = ''){
        $maincategory = Categories::where('parent', null)->get(); 
        $allparent = '';
        $category_parent = '';
        $allparent .= '<option value="none">None</option>';

        if ($category_id > 0) {
            $maincategory = Categories::where('parent', null)->where('id', '!=', $category_id)->get(); 
            $category = Categories::where('id', $category_id)->first();
            $category_parent = $category->parent;
        }

        foreach ($maincategory as $main) {   
            $selected = $main->id == $category_parent ? 'selected' : '';
            $allparent .= '<option '.$selected.' value="'.$main->id.'">'.$main->name.'</option>';
        }

        return $allparent;
    }

    /**
     * Get all category for list on post form.
     * @param  $post_id
     * @return Response
     */
    public static function get_all_category($post_id = 0){
        $selected_cat = [];
        $maincategory = Categories::where('parent', null)->get(); 
        $allcategory = '';

        if ($post_id > 0) {
            $selected_cat = Posts::find($post_id)->categories->pluck('id')->all();
        } 

        foreach ($maincategory as $main) {
            $selected = in_array($main->id, $selected_cat) ? 'checked' : '';
            $allcategory .= '<li><label><input '.$selected.' name="categories[]" type="checkbox" value="'.$main->id.'">'.$main->name.'</label><ul>';
            $subcategory = Categories::where('parent', $main->id)->get(); 
            foreach ($subcategory as $sub) {
                $selected = in_array($sub->id, $selected_cat) ? 'checked' : '';
                $allcategory .= '<li><label><input '.$selected.' name="categories[]" type="checkbox" value="'.$sub->id.'">'.$sub->name.'</label></li>';
            }
            $allcategory .= '</ul></li>';
        }

        return $allcategory;
    }
    
    /**
     * Check post tags input.
     * @param  $tag_input
     * @return value
     */
    public static function check_tags_input($tag_input){
        if (isset($tag_input)) {
            $tags = array();
            foreach ($tag_input as $key) {
                $tag_slug = PostHelper::make_slug($key);
                $check = Tags::where('slug', $tag_slug)->first();
                if (!isset($check)) {
                    // save tag to table tag
                    $save_tag = new Tags;
                    $save_tag->name = $key;
                    $save_tag->slug = $tag_slug;
                    $save_tag->save();
                    $key = $save_tag->id;

                } else {
                  $key = $check->id;
                }
                $tags[] = $key;
            }
        } else {
            $tags = null;
        }

        return $tags;
    }

    /**
     * Get full link file
     * @param  $url, $path
     * @return Response
     */
    public static function getLinkFile($fileName, $path, $inStorage = true){
        if (!empty($path)) {
            $fileName = $path.'/'.$fileName;
        }

        if ($inStorage) {
            return Storage::disk('public')->url("$fileName");
        } else {
            if ( file_exists(public_path()."/$fileName"))
            {
                return env('APP_URL')."/$fileName";
            }
        }
        return '';
    }

    /**
     * Store file to storage
     * @param  $file, $path, $fileName
     * @return Response
     */
    public static function putFile($file, $path, $fileName, $inStorage = true){
        if ($inStorage) {
            $s3 = Storage::disk('public');
            $s3->putFileAs($path, new File($file), $fileName, 'public');
        } else {
            self::deleteFile($fileName, $path, false);
            
            if (!empty($path)) {
                $fileName = $path.'/'.$fileName;
            }

            $myfile = fopen(public_path()."/$fileName", "w") or die("Unable to open file!");
            fwrite($myfile, $file);
            fclose($myfile);

        }
    }

    /**
     * Delete file from storage.
     * @param  $file, $path
     * @return Response
     */
    public static function deleteFile($fileName, $path, $inStorage = true){
        if (!empty($path)) {
            $fileName = $path.'/'.$fileName;
        } 

        if ($inStorage) {
            Storage::disk('public')->delete($fileName);
        } else {
            if ( file_exists(public_path()."/$fileName"))
            {
                unlink(public_path()."/$fileName");
            }
        }
    }

    /**
     * Get full link media.
     * @param  $url, $path, $size = ''
     * @return Response
     */
    public static function getLinkImage($url, $path, $size = ''){
        if ($size != '') {
            if ($size == 'thumbnail') {
                $size = self::$img_thumb;
            } else if ($size == 'medium') {
                $size = self::$img_medium;
            } else if ($size == 'large') {
                $size = self::$img_large;
            }
            $name = explode('.', $url);
            $fileName = $name[0].'-'.$size.'.'.$name[1];
        } else {
            $fileName = $url;
        }

        return Storage::disk('public')->url("$path/$fileName");
    }

    /**
     * Store file to storage.
     * @param  $file, $path, $fileName
     * @return Response
     */
    public static function putImage($file, $path, $fileName){
        $imgObject = Image::make($file);
        $ext = $file->getClientOriginalExtension();
        $large = self::$img_large;
        $medium = self::$img_medium;
        $thumb = self::$img_thumb;

        $img = $imgObject;
        $img = $img->stream($file->getClientOriginalExtension(), 90);

        $imgLarge = $imgObject;
        $imgLarge->resize($large, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        $imgLarge = $imgLarge->stream($file->getClientOriginalExtension(), 90);

        $imgMedium = $imgObject;
        $imgMedium->resize($medium, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        $imgMedium = $imgMedium->stream($file->getClientOriginalExtension(), 90);

        $imgThumb = $imgObject;
        $imgThumb->resize($thumb, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        $imgThumb = $imgThumb->stream($file->getClientOriginalExtension(), 90);

        $storage = Storage::disk('public');
        $filePath = $path.'/' . $fileName.'.'.$ext;
        $storage->put($filePath, $img->__toString(), 'public');

        $filePath = $path.'/' . $fileName. '-'. $large.'.'.$ext;
        $storage->put($filePath, $imgLarge->__toString(), 'public');

        $filePath = $path.'/' . $fileName. '-'. $medium.'.'.$ext;
        $storage->put($filePath, $imgMedium->__toString(), 'public');

        $filePath = $path.'/' . $fileName. '-'. $thumb.'.'.$ext;
        $storage->put($filePath, $imgThumb->__toString(), 'public');
    }
    
    /**
     * Delete media from storage.
     * @param  $file, $path
     * @return Response
     */
    public static function deleteImage($file, $path){
        $large = self::$img_large;
        $medium = self::$img_medium;
        $thumb = self::$img_thumb;
        $ex = explode('.', $file);
        $storage = Storage::disk('public');

        $filePath = $path.'/' . $file;
        $storage->delete($filePath);

        $filePath = $path.'/' . $ex[0].'-'.$thumb.'.'.$ex[1];
        $storage->delete($filePath);

        $filePath = $path.'/' . $ex[0].'-'.$medium.'.'.$ex[1];
        $storage->delete($filePath);

        $filePath = $path.'/' . $ex[0].'-'.$large.'.'.$ex[1];
        $storage->delete($filePath);
    }

    /**
     * delete category.
     * @param  $id
     * @return Response
     */
    public function delete_category($id, $is_bulk = ''){
        $category = Categories::where('id', $id)->first();
        if ($category) {
            DB::beginTransaction();
            try {

                $children = Categories::where('parent', $id)->get();
                if (count($children) > 0) {
                    foreach ($children as $child) {
                        $child->parent = null;
                        $child->update();
                    }
                }

                $category->delete();

                DB::commit();
                if ($is_bulk == 'bulk') {
                    // do nothing
                } else {
                    return redirect(route('panel.category__index'))->with(['msg' => 'Deleted', 'status' => 'success'])->send();
                }
            } catch (\Exception $e) {
                DB::rollback();
                return redirect(route('panel.category__index'))->with(['msg' => 'Delete Error', 'status' => 'danger'])->send();
            }
        }else {
            return redirect(route('panel.category__index'))->with(['msg' => 'Category Not Found', 'status' => 'danger'])->send();
        }
    }

    /**
     * delete tag.
     * @param  $id
     * @return Response
     */
    public function delete_tag($id, $is_bulk = ''){
        $tag = Tags::where('id', $id)->first();
        if ($tag) {
            DB::beginTransaction();
            try {
                $tag->delete();

                DB::commit();
                if ($is_bulk == 'bulk') {
                    // do nothing
                } else {
                    return redirect(route('panel.tag__index'))->with(['msg' => 'Deleted', 'status' => 'success'])->send();
                }
            } catch (\Exception $e) {
                DB::rollback();
                return redirect(route('panel.tag__index'))->with(['msg' => 'Delete Error', 'status' => 'danger'])->send();
            }
        }else {
            return redirect(route('panel.tag__index'))->with(['msg' => 'Tag Not Found', 'status' => 'danger'])->send();
        }
    }

    public static function validation_messages(){
        $messages = [   
            'required' => 'This field is required',
            'image'     => 'The :attribute must be an image (jpeg, png, or gif)',
            'max'       => 'The :attribute maximum size is :max',
            'min'       => 'The :attribute minimum size is :min',
            'mimes'     => 'The :attribute mimes not supported',
            'same'      => 'The :attribute and :other must match.',
            'size'      => 'The :attribute must be exactly :size.',
            'between'   => 'The :attribute must be between :min - :max.',
            'in'        => 'The :attribute must be one of the following types: :values',
            'numeric'   => 'The :attribute must be numeric',
        ];

        return $messages;
    }

    /**
     * Filter keyword slug or title .
     * @param  $text, $title
     * @return Response
     */
    public static function filterKeyword($text, $title){
        $text = str_replace('[judul]', $title, $text);
        return $text;
    }

    /**
     * Get original keyword slug or title .
     * @param  $text, $title
     * @return Response
     */
    public static function unfilterKeyword($text, $title){
        $text = str_replace($title, '[judul]', $text);
        return $text;
    }

    /**
     * Get slug_id for post .
     * @param  $text, $title
     * @return Response
     */
    public static function getSlugID(){
        do {
            $slug_id = self::rand_pseudo(2);
            $check = Posts::where('slug_id', $slug_id)->first();

        } while ($check != null);
        return $slug_id;
    }

    /**
     * random string.
     * @param  $bytes
     * @return Response
     */
    public static function rand_pseudo($bytes){
        $bytes = openssl_random_pseudo_bytes($bytes, $cstrong);
        $hex   = bin2hex($bytes);
        return $hex;
    }

    public static function create_sitemap($check = false){
        // create index sitemap
        if (! file_exists(public_path()."/index-sitemap.xml") || $check){
            self::create_index_sitemap();
        }

        // create post sitemap
        if (! file_exists(public_path()."/post-sitemap.xml") || $check){
            self::create_post_sitemap();
        }

        // create page sitemap
        if (! file_exists(public_path()."/page-sitemap.xml") || $check){
            self::create_page_sitemap();
        }

        // create category sitemap
        if (! file_exists(public_path()."/category-sitemap.xml") || $check){
            self::create_category_sitemap();
        }

        // create tag sitemap
        if (! file_exists(public_path()."/tag-sitemap.xml") || $check){
            self::create_tag_sitemap();
        }
        
        // create keyword global sitemap
        if (! file_exists(public_path()."/keyword-sitemap.xml") || $check){
            self::create_keyword_sitemap();
        }
    }

    /* create index sitemap */
    public static function create_index_sitemap(){
        $sitemaps = ['post-sitemap.xml', 'page-sitemap.xml', 'category-sitemap.xml', 'tag-sitemap.xml', 'keyword-sitemap.xml'];

        $content = '';
        $content .= '<?xml version="1.0"?>   
            <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';

        foreach ($sitemaps as $key => $sitemap) {
            if (!file_exists(public_path()."/$sitemap")) {
                switch ($sitemap) {
                    case 'post-sitemap.xml':
                        self::create_post_sitemap();
                        break;
                    case 'page-sitemap.xml':
                        self::create_page_sitemap();
                        break;
                    case 'category-sitemap.xml':
                        self::create_category_sitemap();
                        break;
                    case 'tag-sitemap.xml':
                        self::create_tag_sitemap();
                        break;
                    case 'keyword-sitemap.xml':
                        self::create_keyword_sitemap();
                        break;
                }
            }
            $timestamp = filemtime(public_path()."/$sitemap");
            $content .= "<sitemap><loc>". url("/$sitemap") ."</loc><lastmod>".date('c',$timestamp)."</lastmod></sitemap>";
        }
        $content .= '</urlset>';

        try {
            self::putFile($content, '', 'index-sitemap.xml', false);   
        } catch (Exception $e) {
            if (config('app.debug')) {
                throw new Exception($e, 1);
            }
            throw new Exception("Error Processing Request", 1);
        }
    }

    /* create post sitemap */
    public static function create_post_sitemap(){
        $posts = Posts::PostType('post')->get();
        $keywords_single = Keywords::keywordType('single')->get();

        $content = '';
        $content .= '<?xml version="1.0"?>   
            <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:news="http://www.google.com/schemas/sitemap-news/0.9" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">';
        $content .= '<url><loc>'.url('/').'</loc></url>';

        foreach ($posts as $key => $post) {
            $content .= "<url><loc>". route("single.post", $post->full_slug) ."</loc><news:news><news:publication><news:name><![CDATA[".config('app.name')."]]></news:name><news:language>id</news:language></news:publication><news:publication_date>".date('c',strtotime($post->published_at))."</news:publication_date><news:title><![CDATA[".$post->meta_title."]]></news:title><news:keywords><![CDATA[".$post->meta_keyword."]]></news:keywords></news:news><image:image><image:loc>".$post->featured_image."</image:loc><image:caption><![CDATA[".$post->featured_image."]]></image:caption></image:image><lastmod>".date('c',strtotime($post->updated_at))."</lastmod></url>";

            foreach ($keywords_single as $key2 => $keyword) {
                $content .= "<url><loc>". route('single.post', self::filterKeyword($keyword->slug, $post->slug).'-'.$post->slug_id) ."</loc><news:news><news:publication><news:name><![CDATA[".config('app.name')."]]></news:name><news:language>id</news:language></news:publication><news:publication_date>".date('c',strtotime($post->published_at))."</news:publication_date><news:title><![CDATA[".self::filterKeyword($keyword->meta_title, $post->title)."]]></news:title><news:keywords><![CDATA[".self::filterKeyword($keyword->meta_keyword, $post->title)."]]></news:keywords></news:news><image:image><image:loc>".$post->featured_image."</image:loc><image:caption><![CDATA[".$post->featured_image."]]></image:caption></image:image><lastmod>".date('c',strtotime($post->updated_at))."</lastmod></url>";
            }

        }
        $content .= '</urlset>';
        try {
            self::putFile($content, '', 'post-sitemap.xml', false);   
        } catch (Exception $e) {
            if (config('app.debug')) {
                throw new Exception($e, 1);
            }
            throw new Exception("Error Processing Request", 1);
        }
    }

    /* create page sitemap */
    public static function create_page_sitemap(){
        $pages = Posts::PostType('page')->get();
        $keywords_single = Keywords::keywordType('single')->get();

        $content = '';
        $content .= '<?xml version="1.0"?>   
            <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
        $content .= '<url><loc>'.url('/').'</loc></url>';

        foreach ($pages as $key => $page) {
            $content .= "<url><loc>". route("single.page", $page->full_slug) ."</loc><lastmod>".date('c',strtotime($page->updated_at))."</lastmod></url>";

            foreach ($keywords_single as $key2 => $keyword) {
                $content .= "<url><loc>". route('single.page', self::filterKeyword($keyword->slug, $page->slug).'-'.$page->slug_id)."</loc><lastmod>".date('c',strtotime($page->updated_at))."</lastmod></url>";
            }
        }
        $content .= '</urlset>';
        try {
            self::putFile($content, '', 'page-sitemap.xml', false);   
        } catch (Exception $e) {
            if (config('app.debug')) {
                throw new Exception($e, 1);
            }
            throw new Exception("Error Processing Request", 1);
        }
    }

    /* create category sitemap */
    public static function create_category_sitemap(){
        $categories = Categories::get();

        $content = '';
        $content .= '<?xml version="1.0"?>   
            <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';

        foreach ($categories as $key => $cat) {
            $post = $cat->posts()->orderBy('updated_at', 'desc')->first();
            $content .= "<url><loc>". route("list.post.category", $cat->slug) ."</loc>";
            if ($post) {
                "<lastmod>".date('c',strtotime($post->updated_at))."</lastmod>";
            }
            $content = "</url>";
        }
        $content .= '</urlset>';
        try {
            self::putFile($content, '', 'category-sitemap.xml', false);   
        } catch (Exception $e) {
            if (config('app.debug')) {
                throw new Exception($e, 1);
            }
            throw new Exception("Error Processing Request", 1);
        }
    }

    /* create tag sitemap */
    public static function create_tag_sitemap(){
        $tags = Tags::get();

        $content = '';
        $content .= '<?xml version="1.0"?>   
            <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';

        foreach ($tags as $key => $tag) {
            $post = $tag->posts()->orderBy('updated_at', 'desc')->first();
            $content .= "<url><loc>". route("list.post.tag", $tag->slug) ."</loc>";
            if ($post) {
                $content .= "<lastmod>".date('c',strtotime($post->updated_at))."</lastmod>";
            }
            $content .= '</url>';
        }
        $content .= '</urlset>';
        try {
            self::putFile($content, '', 'tag-sitemap.xml', false);   
        } catch (Exception $e) {
            if (config('app.debug')) {
                throw new Exception($e, 1);
            }
            throw new Exception("Error Processing Request", 1);
        }
    }

    /* create keyword sitemap */
    public static function create_keyword_sitemap(){
        $keywords = Keywords::keywordType('global')->get();

        $content = '';
        $content .= '<?xml version="1.0"?>   
            <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';

        foreach ($keywords as $key => $keyword) {
            $content .= "<url><loc>". url("/$keyword->slug") ."</loc><lastmod>".date('c',strtotime($keyword->updated_at))."</lastmod></url>";
        }
        $content .= '</urlset>';
        try {
            self::putFile($content, '', 'keyword-sitemap.xml', false);   
        } catch (Exception $e) {
            if (config('app.debug')) {
                throw new Exception($e, 1);
            }
            throw new Exception("Error Processing Request", 1);
        }
    }
}
?>