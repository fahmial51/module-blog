<?php
Route::group(['domain'=>config('admin.domain'),'middleware' => ['web', 'auth'], 'prefix' => 'admin/blog', 'namespace' => 'Modules\Blog\Http\Controllers'], function()
{
    Route::get('/', 'BlogController@dashboard')
            ->name('panel.dashboard');
    Route::get('/index', 'BlogController@dashboard')
            ->name('panel.dashboard__index');

    // post
    Route::get('/posts', 'BlogController@index')
            ->name('panel.post__index');
    Route::get('/ajaxposts', 'BlogController@ajaxPosts')
            ->name('panel.post__index__ajax');
    Route::get('/post/add', 'BlogController@addPost')
            ->name('panel.post__add');
    Route::post('/post/add', 'BlogController@addPostPost')
            ->name('panel.post__save');
    Route::get('/post/{id}/view', 'BlogController@viewPost')
            ->name('panel.post__view');
    Route::post('/post/{id}/update', 'BlogController@updatePost')
            ->name('panel.post__update');
    Route::get('/post/{id}/remove', 'BlogController@removePost')
            ->name('panel.post__delete');
    Route::post('/post/massdelete', 'BlogController@massdeletePost')
            ->name('panel.post__delete__mass');
    
    // pages
    Route::get('/pages', 'BlogController@pages')
            ->name('panel.page__index');
    Route::get('/ajaxpages', 'BlogController@ajaxPages')
            ->name('panel.page__index__ajax');
    Route::get('/page/add', 'BlogController@addPage')
            ->name('panel.page__add');
    Route::post('/page/add', 'BlogController@addPagePost')
            ->name('panel.page__save');
    Route::get('/page/{id}/view', 'BlogController@viewPage')
            ->name('panel.page__view');
    Route::post('/page/{id}/update', 'BlogController@updatePage')
            ->name('panel.page__update');
    Route::get('/page/{id}/remove', 'BlogController@removePage')
            ->name('panel.page__delete');
    Route::post('/page/massdelete', 'BlogController@massdeletePage')
            ->name('panel.page__delete__mass');

    // trash
    Route::get('/trash', 'BlogController@trash')
            ->name('panel.post.trash__index');
    Route::get('/ajaxtrashposts', 'BlogController@ajaxtrashPosts')
                    ->name('panel.post.trash__delete__ajax');
    Route::get('/trash/{id}/delete', 'BlogController@deleteTrash')
            ->name('panel.post.trash__delete');
    Route::get('/trash/{id}/restore', 'BlogController@restoreTrash')
            ->name('panel.post.trash__update__restore');
    Route::post('/trash/massdelete', 'BlogController@massdeleteTrash')
            ->name('panel.post.trash__delete__mass');
    Route::post('/trash/massrestore', 'BlogController@massrestoreTrash')
            ->name('panel.post.trash__update__restore__mass');
    Route::get('/trash/empty', 'BlogController@emptyTrash')
            ->name('panel.post.trash__delete__clear');

    Route::get('/add-category-post/{name}/{parent}', 'BlogController@store_category_ajax')
            ->name('panel.category__add__ajax__post');
    Route::get('/get-category-post/{post_id}', 'BlogController@get_all_category')
            ->name('panel.category__index__get');
    Route::get('/get-category-parent/{category_id}', 'BlogController@get_category_parent')
            ->name('panel.category__index__getparent');

    // categories
    Route::get('/categories', 'BlogController@categories')
            ->name('panel.category__index');    
    Route::get('/ajaxcategories', 'BlogController@ajaxCategories')
            ->name('panel.category__index__ajax');
    Route::get('/category/add', 'BlogController@addCategory')
            ->name('panel.category__add');
    Route::post('/category/add', 'BlogController@addCategoryPost')
            ->name('panel.category__save');
    Route::post('/category/ajaxadd', 'BlogController@addCategoryPost')
            ->name('panel.category__add__ajax');
    Route::get('/category/{id}/view', 'BlogController@viewCategory')
            ->name('panel.category__view');
    Route::post('/category/{id}/update', 'BlogController@updateCategory')
            ->name('panel.category__update');
    Route::get('/category/{id}/remove', 'BlogController@removeCategory')
            ->name('panel.category__delete');
    Route::post('/category/massdelete', 'BlogController@massdeleteCategory')
            ->name('panel.category__delete__mass');

    // tags
    Route::get('/tags', 'BlogController@tags')
            ->name('panel.tag__index');
    Route::get('/ajaxtags', 'BlogController@ajaxTags')
            ->name('panel.tag__index__ajax');
    Route::get('/tag/add', 'BlogController@addTag')
            ->name('panel.tag__add');
    Route::post('/tag/add', 'BlogController@addTagPost')
            ->name('panel.tag__save');
    Route::get('/tag/{id}/view', 'BlogController@viewTag')
            ->name('panel.tag__view');
    Route::post('/tag/{id}/update', 'BlogController@updateTag')
            ->name('panel.tag__update');
    Route::get('/tag/{id}/remove', 'BlogController@removeTag')
            ->name('panel.tag__delete');
    Route::post('/tag/massdelete', 'BlogController@massdeleteTag')
            ->name('panel.tag__delete__mass');

    // media
    Route::get('/media', 'BlogController@media')
            ->name('panel.media__index');
    Route::get('/get-media', 'BlogController@get_media')
            ->name('panel.media__index__ajax');
    Route::post('/get-media', 'BlogController@get_media')
            ->name('panel.media__index__ajax__post');
    Route::get('/delete-media/{id}', 'BlogController@destroy_media')
            ->name('panel.media__delete');
    Route::post('/store-media', 'BlogController@store_media')
            ->name('panel.media__save');
    Route::post('/bulk-delete-media/', 'BlogController@bulk_delete_media')
            ->name('panel.media__delete__mass');

    Route::get('/site-setting', 'BlogController@site_setting_view')
            ->name('panel.setting.site__index');
    Route::post('/save-setting', 'BlogController@site_setting_save')
            ->name('panel.setting.site__update');
    Route::post('/save-program', 'BlogController@save_program')
            ->name('panel.setting.site__update__program');

    // slider
    Route::get('/slider','BlogController@head_slider')
            ->name('panel.slider__index');
    Route::get('/new-slider','BlogController@new_slider_view')
            ->name('panel.slider__add');
    Route::post('/act-new-slider','BlogController@new_slider_act')
            ->name('panel.slider__save');
    Route::get('/edit-slider/{id}','BlogController@edit_slider_view')
            ->name('panel.slider__view');
    Route::post('/edit-slider/{id}/act_edit_slider','BlogController@edit_slider_act')
            ->name('panel.slider__update');
    Route::get('/hapus-slider/{id}','BlogController@hapus_slider')
            ->name('panel.slider__delete');

    // keywords
    Route::get('/keywords', 'BlogController@keywords')
            ->name('panel.keywords__index');
    Route::get('/get-keywords', 'BlogController@ajaxKeywords')
            ->name('panel.keywords__index__ajax');
    Route::post('/get-keywords', 'BlogController@ajaxKeywords')
            ->name('panel.keywords__index__ajax');
    Route::get('/keywords/add', 'BlogController@addKeyword')
            ->name('panel.keywords__add');
    Route::post('/keywords/add', 'BlogController@addKeywordPost')
            ->name('panel.keywords__save');
    Route::get('/keywords/{id}/view', 'BlogController@viewKeyword')
            ->name('panel.keywords__view');
    Route::post('/keywords/{id}/update', 'BlogController@updateKeyword')
            ->name('panel.keywords__update');
    Route::get('/keywords/{id}/remove', 'BlogController@removeKeyword')
            ->name('panel.keywords__delete');
    Route::post('/keywords/massdelete', 'BlogController@massdeleteKeywords')
            ->name('panel.keywords__delete__mass');

    // users
    Route::get('/users', 'BlogController@users')
            ->name('panel.users__index');
    Route::get('/get-users', 'BlogController@ajaxUsers')
            ->name('panel.users__index__ajax');
    Route::post('/get-users', 'BlogController@ajaxUsers')
            ->name('panel.users__index__ajax');
    Route::get('/users/add', 'BlogController@addUser')
            ->name('panel.users__add');
    Route::post('/users/add', 'BlogController@addUserPost')
            ->name('panel.users__save');
    Route::get('/users/{id}/view', 'BlogController@viewUser')
            ->name('panel.users__view');
    Route::post('/users/{id}/update', 'BlogController@updateUser')
            ->name('panel.users__update');
    Route::get('/users/{id}/remove', 'BlogController@removeUser')
            ->name('panel.users__delete');
});

Route::group(['middleware' => 'web', 'prefix' => 'doc-ui', 'namespace' => 'Modules\Blog\Http\Controllers'], function(){
    Route::get('/','DocController@index');
});

Route::group(['middleware' => 'web', 'prefix' => 'blogs', 'namespace' => 'Modules\Blog\Http\Controllers'], function(){
    Route::get('/','FrontController@index');
    Route::get('/read/{slug_id}','FrontController@single_post')->name('single.post');
    Route::get('/{slug_id}','FrontController@single_page')->name('single.page');
    Route::get('/category/{category_slug}','FrontController@postsCategory')->name('list.post.category');
    Route::get('/tag/{tag_slug}','FrontController@postsTag')->name('list.post.tag');

    //Comment
    Route::post('/go-comment/{id}','FrontController@doInsertComment');
    Route::get('/getcomment/{postid}','FrontController@getPostComment');
});

Route::get('/admin', function(){
    return Redirect::to('/admin/blog');
});

Route::get('/create-sitemap', function(){
    PostHelper::create_sitemap();
    return Redirect::to('/');
});

Route::get('/recreate-sitemap', function(){
    PostHelper::create_sitemap(true);
    return Redirect::to('/');
});

Route::get('index-sitemap.xml', function(){
    PostHelper::create_index_sitemap();
    return Redirect::to('index-sitemap.xml');
});

Route::get('post-sitemap.xml', function(){
    PostHelper::create_post_sitemap();
    return Redirect::to('post-sitemap.xml');
});

Route::get('page-sitemap.xml', function(){
    PostHelper::create_page_sitemap();
    return Redirect::to('page-sitemap.xml');
});

Route::get('category-sitemap.xml', function(){
    PostHelper::create_category_sitemap();
    return Redirect::to('category-sitemap.xml');
});

Route::get('tag-sitemap.xml', function(){
    PostHelper::create_tag_sitemap();
    return Redirect::to('tag-sitemap.xml');
});

Route::get('keyword-sitemap.xml', function(){
    PostHelper::create_keyword_sitemap();
    return Redirect::to('keyword-sitemap.xml');
});
