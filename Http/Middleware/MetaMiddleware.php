<?php

namespace Modules\Blog\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Modules\Blog\Entities\Option;
use Modules\Blog\Entities\Keywords;

class MetaMiddleware
{
    public function handle($request, Closure $next)
    {
        // Perform action
        app()->instance('Meta', new Meta);

        return $next($request);
    }
}

/**
* 
*/
class Meta
{
    protected $meta_site_name = '';
    protected $default_meta_title = '';
    protected $default_meta_desc = '';
    protected $default_meta_keyword = '';
    protected $meta_title = '';
    protected $meta_desc = '';
    protected $meta_keyword = '';
    protected $meta_type = 'website';
    protected $meta_url = '';
    protected $meta_image = '';
    protected $meta = array();
    protected $email_info = '';
    protected $keywords_single = [];
    protected $keywords_global = [];
    protected $default_schema = '';
    protected $nav_schema = '';
    protected $breadcrumb_schema = '';
    protected $gtm = '';
    protected $fb_pixel = '';

    function __construct(){
        $this->fb_pixel = Option::where('meta_key', 'fb_pixel')->first()->meta_value ?? '';
        $this->gtm = Option::where('meta_key', 'gtag_manager')->first()->meta_value ?? '';

        $meta_title = Option::where('meta_key', 'default_meta_title')->first()->meta_value ?? '';
        $meta_desc = Option::where('meta_key', 'default_meta_desc')->first()->meta_value ?? '';
        $meta_keyword = Option::where('meta_key', 'default_meta_keyword')->first()->meta_value ?? '';

        $this->meta_site_name = config('app.name');
        $this->default_meta_title = ucwords($meta_title);
        $this->default_meta_desc = $meta_desc;
        $this->default_meta_keyword = ucwords($meta_keyword);
        $this->meta_title = ucwords($meta_title);
        $this->meta_desc = $meta_desc;
        $this->meta_keyword = ucwords($meta_keyword);
        $this->meta_url = url()->current();
        $this->meta_image = asset('images/logo-warna.png');
        $this->keywords_single = Keywords::keywordType('single')->get();
        $this->keywords_global = Keywords::keywordType('global')->get();

        $desc           = $meta_desc;
        $default_schema = array();
        $organization   = array();
        $brand          = array();

        // setup array data 
        $default_schema['@context'] = 'http://schema.org';

        // get data organization
        $organization['@type']          = 'Organization';
        $organization['name']           = config('app.name');
        $organization['url']            = url('/');
        $organization['logo']           = '';
        $organization['description']    = $desc;
        $organization['ContactPoint']['@type']              = 'ContactPoint';
        $organization['ContactPoint']['telephone']          = '+6281721983567';
        $organization['ContactPoint']['contactType']        = 'customer service';
        $organization['ContactPoint']['availableLanguage']  = array("English","Indonesian");

        // get data brand
        $brand['@type']       = 'Brand';
        $brand['url']         = url('/');
        $brand['name']        = config('app.name');
        $brand['description'] = $desc;

        $default_schema['@graph'][] = $organization;
        $default_schema['@graph'][] = $brand;

        $this->default_schema = json_encode($default_schema);

        $nav_schema['@context'] = 'http://schema.org';
        $nav_schema['@graph'][0]['@type']     = "SiteNavigationElement";
        $nav_schema['@graph'][0]['@id']       = "#primary";
        $nav_schema['@graph'][0]['name']      = "Home";
        $nav_schema['@graph'][0]['url']       = url('/');
        $nav_schema['@graph'][1]['@type']     = "SiteNavigationElement";
        $nav_schema['@graph'][1]['@id']       = "#primary";
        $nav_schema['@graph'][1]['name']      = "Jackpot";
        $nav_schema['@graph'][1]['url']       = url('/jackpot');
        $nav_schema['@graph'][2]['@type']     = "SiteNavigationElement";
        $nav_schema['@graph'][2]['@id']       = "#primary";
        $nav_schema['@graph'][2]['name']      = "Download";
        $nav_schema['@graph'][2]['url']       = url('/download');
        $nav_schema['@graph'][3]['@type']     = "SiteNavigationElement";
        $nav_schema['@graph'][3]['@id']       = "#primary";
        $nav_schema['@graph'][3]['name']      = "Promotion";
        $nav_schema['@graph'][3]['url']       = url('/promotion');
        $nav_schema['@graph'][4]['@type']     = "SiteNavigationElement";
        $nav_schema['@graph'][4]['@id']       = "#primary";
        $nav_schema['@graph'][4]['name']      = "Vip";
        $nav_schema['@graph'][4]['url']       = url('/vip');

        $this->nav_schema = json_encode($nav_schema);

        $breadcrumb_schema['@context']   = 'http://schema.org';
        $breadcrumb_schema['@type']      = 'BreadcrumbList';

        $position[0]['@type']        = 'ListItem';
        $position[0]['position']     = 1;
        $position[0]['item']['@id']  = url('/');
        $position[0]['item']['name'] = 'Home';
        $position[0]['item']['url']  = url('/');

        if (url()->current() != url()) {
            $position[1]['@type']        = 'ListItem';
            $position[1]['position']     = 2;
            $position[1]['item']['@id']  = url()->current();
            $position[1]['item']['name'] = ucwords(str_replace(url('/').'/', '', $position[1]['item']['@id']));
            $position[1]['item']['url']  = url()->current();
        }

        $breadcrumb_schema['itemListElement'] =  $position;
        $this->breadcrumb_schema = json_encode($breadcrumb_schema);

    }

    public  function set($var, $value){
        if ($var == 'breadcrumb_schema') {
            $breadcrumb_schema['@context']   = 'http://schema.org';
            $breadcrumb_schema['@type']      = 'BreadcrumbList';

            $position[0]['@type']        = 'ListItem';
            $position[0]['position']     = 1;
            $position[0]['item']['@id']  = url('/');
            $position[0]['item']['name'] = 'Home';
            $position[0]['item']['url']  = url('/');
            if (is_array($value) && sizeof($value) > 0) {
                foreach ($value as $key => $val) {
                    $position[$key]['@type']        = 'ListItem';
                    $position[$key]['position']     = $key+2;
                    $position[$key]['item']['@id']  = $val['url'];
                    $position[$key]['item']['name'] = $val['name'];
                    $position[$key]['item']['url']  = $val['url'];
                }
            }
            $breadcrumb_schema['itemListElement'] =  $position;
            $this->breadcrumb_schema = json_encode($breadcrumb_schema);
        }

        if (is_array($var) && sizeof($var) > 0) {
            foreach ($var as $key => $var) {
                $this->{$var} = $value;
            }
        } else {
            $this->{$var} = $value;   
        }
    }

    public function get($var){
        return $this->{$var};
    }

    public function print_meta($useprefix=true){
        $this->meta = array('title' => ucwords($this->meta_title),
                             'description' => $this->meta_desc,
                             'keyword' => $this->meta_keyword,
                             'og:type' => $this->meta_type,
                             'og:title' => ucwords($this->meta_title),
                             'og:description' => $this->meta_desc,
                             'og:url' => $this->meta_url,
                             'og:image' => $this->meta_image,
                             'og:image:alt' => ucwords($this->meta_title),
                             'og:site_name' => ucwords($this->meta_site_name),
                             'twitter:card' => 'summary',
                             'twitter:description' => $this->meta_desc,
                             'twitter:title' => ucwords($this->meta_title),
                             'twitter:url' => $this->meta_url,
                             'twitter:image:src' => $this->meta_image
                            );
        $element = '';
        foreach ($this->meta as $key => $value) {
            if (strpos($key, 'twitter') !== false) {
                $element .= "<meta name='$key' content='$value'>\n";   
            } else if (strpos($key, ':') !== false) {
                $element .= "<meta property='$key' content='$value'>\n";   
            } else {
                $element .= "<meta name='$key' content='$value'>\n";   
            }   
        }
        return $element;
    }
}