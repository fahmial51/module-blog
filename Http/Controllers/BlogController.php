<?php

namespace Modules\Blog\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Input;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Routing\Controller as BaseController;
use App\Http\Controllers\Controller;
use Modules\Blog\Entities\Posts;
use Modules\Blog\Entities\Comments;
use Modules\Blog\Entities\PostMeta;
use Modules\Blog\Entities\Categories;
use Modules\Blog\Entities\Tags;
use Modules\Blog\Entities\PostCategories;
use Modules\Blog\Entities\PostTags;
use Modules\Blog\Entities\Media;
use Modules\Blog\Entities\Option;
use Modules\Blog\Entities\Slider;
use Modules\Blog\Entities\Keywords;
use Modules\Blog\Http\Helpers\PostHelper;
use App\User;
use Carbon\Carbon;
use Auth;
use DB;
use File;
use Image;
use View;
use Validator;


class BlogController extends Controller
{
    private $prefix;

    public function __construct(){
        $this->PostHelper = new PostHelper;
        $this->prefix = 'admin/blog/';
        View::share('prefix', $this->prefix);
        View::share('body_id', 'blog');
        View::share('tinymceApiKey', config('app.tinymce_api_key'));
    }
    
    /**
     * Display a listing of post (Dashboard).
     * @return Response
     */
    public function dashboard(){
        $page_meta_title = 'Posts';
        return view('blog::admin.index')->with(['page_meta_title' => $page_meta_title]);
    }

    /**
     * Display a listing of post.
     * @return Response
     */
    public function index(){
        $page_meta_title = 'Posts';
        return view('blog::admin.index')->with(['page_meta_title' => $page_meta_title]);
    }

    /**
     * Get posts for datatable(ajax).
     * @param  Request $request
     * @return Response
     */
    public function ajaxposts(Request $request)
    {
        $order = $request->order[0];
        $col = $request->columns["{$order['column']}"]['data'] ?? 'created_at'; 
        $direction = $order['dir'] ?? 'desc';
        
        $query = Posts::where('post_type','post')->orderBy($col,$direction);
        $search = $request->search['value'];
        if (isset($search)) {
            $query = $query->where('title', 'like', '%'.$search.'%');   
        }
        $output['data'] = $query->offset($request['start'])
                                ->limit($request['length'])
                                ->get();

        foreach ($output['data'] as $key => $data) {
            $user = Posts::find($data->id)->user;
            if (!$user) {
                $user = new \stdClass;
                $user->username = 'admin';
            } 
            $data->author = $user;
        }

        $output['recordsTotal'] = $query->count();
        $output['recordsFiltered'] = $output['recordsTotal'];
        $output['draw'] = intval($request->input('draw'));
        $output['length'] = 10;

        return $output;
    }

    /**
     * Show the form for creating a new post.
     * @return Response
     */
    public function addPost()
    {
        $page_meta_title = 'Posts';
        $alltag = Tags::orderBy('created_at','desc')->get();

        return view('blog::admin.post_add')->with(['page_meta_title' => $page_meta_title, 'alltag' => $alltag]);
    }

    /**
     * Store a newly created post in storage.
     * @param  Request $request
     * @return Response
     */
    public function addPostPost(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'content' => 'required'
        ], PostHelper::validation_messages());

        $categories = $request->input('categories') ?? [] ;
        $meta_title = $request->input('meta_title');
        $meta_desc = $request->input('meta_desc');
        $meta_keyword = $request->input('meta_keyword');

        $slug = PostHelper::make_slug($request->input('title'));
        if (Posts::withTrashed()->where('slug', $slug)->first()) {
            $slug = $slug.'-'.date('s');
        }

        $published_at = $request->input('published_at');
        if ($published_at == 'immediately') {
            $published_at = Carbon::now()->toDateTimeString();
        }
        DB::beginTransaction();
        try {
            $tag_input = $request->input('tags') ?? [];
            $tags = PostHelper::check_tags_input($tag_input);

            $store = new Posts;
            $store->slug_id = PostHelper::getSlugID();
            $store->slug = $slug;
            $store->title = $request->input('title');
            $store->content = $request->input('content');
            $store->featured_image = $request->input('featured_image');
            $store->post_type = 'post';
            $store->status = $request->get('status');
            $store->author = Auth::user()->id;
            $store->published_at = $published_at;
            $store->save();

            $meta_contents = array();
            $metas[] = ['name' => 'meta_title', 'value' => $meta_title];
            $metas[] = ['name' => 'meta_desc', 'value' => $meta_desc];
            $metas[] = ['name' => 'meta_keyword', 'value' => $meta_keyword];
            $post_category = [];
            foreach ($categories as $cat) {
                $post_category[] = ['post_id' => $store->id, 'category_id' => $cat];
            }
            $post_tag = [];
            foreach ($tags as $tag) {
                $post_tag[] = ['post_id' => $store->id, 'tag_id' => $tag];
            }

            foreach ($metas as $meta) {
                if ($meta['value'] != '') {
                    $meta_contents[] = [ 'post_id'=>$store->id, 'meta_key'=> $meta['name'], 'meta_value'=> $meta['value'] ];
                }
            }

            PostMeta::insert($meta_contents);
            PostTags::insert($post_tag);
            PostCategories::insert($post_category);
            
            DB::commit();
            return redirect(route('panel.post__view', $store->id))->with(['msg' => 'Saved', 'status' => 'success']);
        } catch (\Exception $e) {
            DB::rollback();
            if (config('app.debug')) {
                return redirect(route('panel.post__add'))->with(['msg' => 'Save Error : '.$e, 'status' => 'danger'])->withInput();
            }

            return redirect(route('panel.post__add'))->with(['msg' => 'Save Error', 'status' => 'danger'])->withInput();
        }
    }

    /**
     * Show the form for editing post.
     * @param $id
     * @return Response
     */
    public function viewPost($id)
    {
        $page_meta_title = 'Posts';
        $post = Posts::find($id);
        if ($post) {
            $post_meta = Posts::find($id)->metas->pluck('meta_value', 'meta_key')->all();
            $post_tags = Posts::find($id)->tags->pluck('name')->all();
            $alltag = Tags::get();
            $item_id = $post->id;

            return view('blog::admin.post_edit')->with([ 'item_id' => $item_id, 
                                                         'page_meta_title' => $page_meta_title, 
                                                         'post' => $post , 
                                                         'alltag' => $alltag, 
                                                         'post_tags' => $post_tags,
                                                         'post_meta' => (object)$post_meta 
                                                        ]);
        } else {
            return redirect(route('panel.post__index'))->with(['msg' => 'Post Not Found', 'status' => 'danger']);
        }
    }

    /**
     * Update the specified post in storage.
     * @param  Request $request, $id
     * @return Response
     */
    public function updatePost(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'content' => 'required'
        ], PostHelper::validation_messages());

        $categories = $request->input('categories') ?? [] ;
        $meta_title = $request->input('meta_title');
        $meta_desc = $request->input('meta_desc');
        $meta_keyword = $request->input('meta_keyword');

        $published_at = $request->input('published_at');
        if ($published_at == 'immediately') {
            $published_at = Carbon::now()->toDateTimeString();
        }

        DB::beginTransaction();
        try {
            $tag_input = $request->input('tags') ?? [];
            $tags = PostHelper::check_tags_input($tag_input);

            $update = Posts::where('id', $id)->first();
            $update->title = $request->input('title');
            $update->content = $request->input('content');
            $update->featured_image = $request->input('featured_image');
            $update->status = $request->input('status');
            $update->published_at = $published_at;
            $update->update();
            
            PostMeta::where('post_id',$update->id)->delete();

            $meta_contents = array();
            $metas[] = ['name' => 'meta_title', 'value' => $meta_title];
            $metas[] = ['name' => 'meta_desc', 'value' => $meta_desc];
            $metas[] = ['name' => 'meta_keyword', 'value' => $meta_keyword];
            $post_category = [];
            foreach ($categories as $cat) {
                $post_category[] = ['post_id' => $update->id, 'category_id' => $cat];
            }
            $post_tag = [];
            foreach ($tags as $tag) {
                $post_tag[] = ['post_id' => $update->id, 'tag_id' => $tag];
            }

            foreach ($metas as $meta) {
                if ($meta['value'] != '') {
                    $meta_contents[] = [ 'post_id'=>$update->id, 'meta_key'=> $meta['name'], 'meta_value'=> $meta['value'] ];
                }
            }

            PostMeta::insert($meta_contents);
            PostTags::insert($post_tag);
            PostCategories::insert($post_category);

            DB::commit();
            return redirect(route('panel.post__view', $update->id))->with(['msg' => 'Saved', 'status' => 'success']);
        } catch (\Exception $e) {
            DB::rollback();
            if (config('app.debug')) {
                return redirect(route('panel.post__view', $id))->with(['msg' => 'Save error : '.$e, 'status' => 'danger']);
            }
            return redirect(route('panel.post__view', $id))->with(['msg' => 'Save error', 'status' => 'danger']);
        }
    }

    /**
     * Change post status delete to 1.
     * @param $id
     * @return Response
     */
    public function removePost($id)
    {
        $delete = Posts::find($id);
        if ($delete){
            $delete->delete();
            return redirect(route('panel.post__index'))->with(['msg' => 'Deleted', 'status' => 'success']);
        }
        return redirect(route('panel.post__index'))->with(['msg' => 'Delete error', 'status' => 'danger']);
    }

    /**
     * Change multiple post status delete to 1.
     * @param  Request $request
     * @return Response
     */
    public function massdeletePost(Request $request)
    {
        $id = json_decode($request->id);
        foreach ($id as $id) {
            $delete = Posts::find($id);
            if ($delete) {
                if (!$delete->delete()) {
                    return redirect(route('panel.post__index'))->with(['msg' => 'Delete Error', 'status' => 'danger']);
                }
            } else {
                return redirect(route('panel.post__index'))->with(['msg' => 'Delete Error. Post Not Found', 'status' => 'danger']);
            }
        }
        return redirect(route('panel.post__index'))->with(['msg' => 'Delete Success', 'status' => 'success']);
    }

    // tags

    public function tags(){
        $page_meta_title = 'Tags';
        $data = Tags::get();
        return view('blog::admin.tags')->with(['page_meta_title' => $page_meta_title, 'tags' => $data]);
    }

    public function ajaxTags(Request $request){
        $order = $request->order[0];
        $col = $request->columns["{$order['column']}"]['data'] ?? 'created_at'; 
        $direction = $order['dir'] ?? 'desc';
        
        $query = Tags::orderBy($col,$direction);
        $search = $request->search['value'];
        if (isset($search)) {
            $query = $query->where('name', 'like', '%'.$search.'%');   
        }
        $output['data'] = $query->offset($request['start'])->limit($request['length'])->get();
        $output['recordsTotal'] = $query->count();
        $output['recordsFiltered'] = $output['recordsTotal'];
        $output['draw'] = intval($request->input('draw'));
        $output['length'] = 10;

        return $output;
    }

    public function addTag()
    {
        $page_meta_title = 'Tag';
        $act = 'New';
        $name = '';
        return view('blog::admin.tagform')->with(['page_meta_title' => $page_meta_title, 'act' => $act, 'name' => $name, 'isEdit'=>false]);
    }

    public function addTagPost(Request $request)
    {
        $slug = PostHelper::make_slug($request->input('name'));
        $store = new Tags;
        $store->name = $request->input('name');
        $store->slug = $slug;
        if ($store->save()){
            return redirect(route('panel.tag__view', $store->id))->with(['msg' => 'Saved', 'status' => 'success']);
        } else {
            return redirect(route('panel.tag__index'))->with(['msg' => 'Save Error', 'status' => 'danger']);
        }
    }

    /**
     * edit category
     * @param $id
     * @return Response
     */
    public function viewTag($id){
        $page_meta_title = 'Tag';
        $act = 'Edit';
        $tag = Tags::where('id', $id)->first();
        if (isset($tag)) {
            $name = $tag->name;
            return view('blog::admin.tagform')->with(['id'=>$id, 'page_meta_title' => $page_meta_title, 'act' => $act, 'tag' => $tag, 'name' => $name, 'isEdit'=>true]);
        } else {
            return redirect(route('panel.tag__index'))->with('msg', 'Tag Not Found')->with('status', 'danger');
        }
    }

    /**
     * Update category
     * @param  Request $request, $id
     * @return Response
     */
    public function updateTag(Request $request, $id){
        $update = Tags::where('id', $id)->first();
        $update->name = $request->input('name');
        if ($update->save()){
            return redirect(route('panel.tag__view', $update->id))->with(['msg' => 'Saved', 'status' => 'success']);
        } else {
            return redirect(route('panel.tag__view', $id))->with(['msg' => 'Save Error', 'status' => 'danger']);
        }
    }

    /**
     * remove Category
     *
     * @return void
     * @author 
     **/
    public function removeTag($id){
        $this->PostHelper->delete_tag($id);
    }

    /**
     * mass delete cat
     *
     * @return void
     * @author 
     **/
    public function massdeleteTag(Request $request)
    {
        $id = json_decode($request->id);
        foreach ($id as $id) {
            $this->PostHelper->delete_tag($id, 'bulk');
        }
        return redirect(route('panel.tag__index'))->with(['msg' => 'Delete Success', 'status' => 'success']);
    }


    // categories

    public function categories(){
        $page_meta_title = 'Categories';
        $categories = Categories::get();
        return view('blog::admin.categories')->with(['page_meta_title' => $page_meta_title, 'categories' => $categories]);
    }

    public function ajaxCategories(Request $request){
        $order = $request->order[0];
        $col = $request->columns["{$order['column']}"]['data'] ?? 'created_at'; 
        $direction = $order['dir'] ?? 'desc';
        
        $query = Categories::orderBy($col,$direction);
        $search = $request->search['value'];
        if (isset($search)) {
            $query = $query->where('name', 'like', '%'.$search.'%');   
        }
        $output['data'] = $query->offset($request['start'])->limit($request['length'])->get();
        $output['recordsTotal'] = $query->count();
        $output['recordsFiltered'] = $output['recordsTotal'];
        $output['draw'] = intval($request->input('draw'));
        $output['length'] = 10;

        return $output;
    }

    public function addCategory()
    {
        $page_meta_title = 'Category';
        $act = 'New';
        $name = '';
        $desc = '';
        $allparent = PostHelper::get_category_parent();
        return view('blog::admin.catform')->with(['page_meta_title' => $page_meta_title, 'act' => $act, 'name' => $name,'desc'=>$desc, 'allparent' => $allparent , 'isEdit'=>false]);
    }

    public function addCategoryPost(Request $request)
    {
        $parent = $request->get('parent');
        if ($parent == 'none') {
            $parent = null;
        }
        $slug = PostHelper::make_slug($request->input('name'));
        $store = new Categories;
        $store->name = $request->input('name');
        $store->description = $request->input('description') ?? '';
        $categoryajax = $request->input('catjax') ?? false;
        $store->slug = $slug;
        $store->parent = $parent;
        if ($store->save()){

            if ( $categoryajax ){
                return response("<li><label><input checked selected name='categories[]' type='checkbox' value='$store->id'>$store->name</label></li>");
            }

            return redirect(route('panel.category__view', $store->id))->with(['msg' => 'Saved', 'status' => 'success']);

        } else {
            return redirect(route('panel.category__index'))->with(['msg' => 'Save Error', 'status' => 'danger']);
        }
    }

    /**
     * edit category
     * @param $id
     * @return Response
     */
    public function viewCategory($id){
        $page_meta_title = 'Category';
        $act = 'Edit';
        $action = $this->prefix.'update-category/'.$id;
        $category = Categories::where('id', $id)->first();
        // dd($category->id);
        if (isset($category)) {
            $maincategory = Categories::where('parent', null)->get(); 
            $allparent = PostHelper::get_category_parent($category->id);
            $name = $category->name;
            $desc = $category->description;
            $category_id = $category->id;
            return view('blog::admin.catform')->with(['category_id' => $category_id, 'page_meta_title' => $page_meta_title, 'act' => $act, 'action' => $action,'desc'=>$desc, 'category' => $category, 'name' => $name, 'allparent' => $allparent,'isEdit'=>true]);
        }else {
            return redirect(route('panel.category__index'))->with(['msg' => 'Category Not Found', 'status' => 'danger']);
        }
    }

    /**
     * Update category
     * @param  Request $request, $id
     * @return Response
     */
    public function updateCategory(Request $request, $id){
        $parent = $request->get('parent');
        if ($parent == 'none') {
            $parent = null;
        }
        $update = Categories::where('id', $id)->first();
        $update->name = $request->input('name');
        $update->description = $request->input('description') ?? '';
        $update->parent = $parent;
        if ($update->save()){
            return redirect(route('panel.category__view', $update->id))->with(['msg' => 'Saved', 'status' => 'success']);
        } else {
            return redirect(route('panel.category__view', $id))->with(['msg' => 'Save Error', 'status' => 'danger']);
        }
        return redirect(route('panel.category__index'));
    }

    /**
     * remove Category
     *
     * @return void
     * @author 
     **/
    public function removeCategory($id){
        $this->PostHelper->delete_category($id);
    }

    /**
     * mass delete cat
     *
     * @return void
     * @author 
     **/
    public function massdeleteCategory(Request $request)
    {
        $id = json_decode($request->id);
        foreach ($id as $id) {
            $this->PostHelper->delete_category($id, 'bulk');
        }
        return redirect(route('panel.category__index'))->with(['msg' => 'Delete Success', 'status' => 'success']);
    }

    /**
     * Get categories for datatable(ajax).
     * @param  Request $request
     * @return Response
     */
    public function get_category(Request $request){
        $order = $request->order[0];
        $col = $request->columns["{$order['column']}"]['data'] ?? 'created_at'; 
        $direction = $order['dir'] ?? 'desc';
        
        $query = Category::orderBy($col,$direction);
        $search = $request->search['value'];
        if (isset($search)) {
            $query = $query->where('name', 'like', '%'.$search.'%');   
        }
        $output['data'] = $query->offset($request['start'])->limit($request['length'])->get();
        $output['recordsTotal'] = $query->count();
        $output['recordsFiltered'] = $output['recordsTotal'];
        $output['draw'] = intval($request->input('draw'));
        $output['length'] = 10;

        return $output;
    }

    /**
     * Show the form for creating a new category.
     * @return Response
     */
    public function create_category(){
        $page_meta_title = 'Category';
        $act = 'New';
        $action = $this->prefix.'store-category';
        $name = ''; 
        $allparent = PostHelper::get_category_parent();
        return view('blog::admin.category_form')->with(['page_meta_title' => $page_meta_title, 'act' => $act, 'action' => $action, 'name' => $name, 'allparent' => $allparent]);
    }

    /**
     * Store a newly created category in storage. Ajax function.
     * @param  $name, $parent
     * @return Response
     */
    public function store_category_ajax($name, $parent){
        if ($parent == 'none') {
            $parent = null;
        }
        $slug = PostHelper::make_slug($name);
        $store = new Categories;
        $store->name = $name;
        $store->slug = $slug;
        $store->description = '';
        $store->parent = $parent;
        if ($store->save()){
            return 'success saving category';
        } else {
            return 'error saving category';
        }
    }

    /**
     * Display a listing of media.
     * @return Response
     */
    public function media(){
        $page_meta_title = 'Media';
        return view('blog::admin.media')->with(['page_meta_title' => $page_meta_title]);
    }

    /**
     * Get medias for datatable(ajax).
     * @param  Request $request
     * @return Response
     */
    public function get_media(Request $request){
        $order = $request->order[0];
        $col = $request->columns["{$order['column']}"]['data'] ?? 'created_at'; 
        $direction = $order['dir'] ?? 'desc';

        $medias = Media::orderBy($col,$direction);
        $output['recordsTotal'] = $medias->count();
        $search = $request->search['value'];
        if (isset($search)) {
            $medias = $medias->where('name', 'like', '%'.$search.'%');   
        }
        $output['data'] = $medias->offset($request['start'])->limit($request['length'])->get();
        $output['recordsFiltered'] = $output['recordsTotal'];
        $output['draw'] = intval($request->input('draw'));
        $output['length'] = $request['length'];
        $output['start']=$request['start'];

        return $output;
    }

    /**
     * Store a newly created media in storage.
     * @param  Request $request
     * @return Response
     */
    public function store_media(Request $req){
        $this->validate($req, [
            'media.*' => 'image|max:3000|mimes:jpg,jpeg,png,gif',
        ], PostHelper::validation_messages());
        if ($req->hasFile('media')) {
            try {
                $file = $req->file('media');
                foreach ($file as $file) {
                    $fileName = time();
                    $name = $fileName.'.'.$file->getClientOriginalExtension();
                    $name = strtolower($name);

                    PostHelper::putImage($file, 'media', $fileName);

                    $media = new Media();
                    $media->name = $name;
                    $media->save();
                    echo "Success adding media ".$name;
                }
            } catch (\Exception $e) {
                if (config('app.debug')) {
                    return "Something went wrong : ".$e;
                }
                return "Something went wrong";
            }
        }else{
            return  "Failed adding media";
        }
    }

    /**
     * Remove the specified media from storage.
     * @param $id
     * @return Response
     */
    public function destroy_media($id){
        $media = Media::where('id',$id)->first();
        if($media->name != ""){
            if(Storage::disk('public')->exists('media/'.$media->name)){
                $this->PostHelper->deleteImage($media->name, 'media');
            }
        }
        if ($media -> delete()) {
            return "Media deleted";
        }else{
            return "failed deleting media";
        }
    }

    /**
     * Remove multiple media from storage.
     * @param Request $request
     * @return Response
     */
    public function bulk_delete_media(Request $request)
    {
        $id = json_decode($request->id);
        foreach ($id as $id) {
            $media = Media::where('id', $id)->first();
            if (isset($media)) {
                if($media->name != ""){
                    if(Storage::disk('public')->exists('media/'.$media->name)){
                        $this->PostHelper->deleteImage($media->name, 'media');
                    }
                }
                if ($media->delete()) {
                    // do nothing
                } else {
                    return redirect($this->prefix.'media')->with(['msg' => 'Delete Error', 'status' => 'danger']);
                }
            } else {
                return redirect($this->prefix.'media')->with(['msg' => 'Delete Error.Some Media Not Found', 'status' => 'danger']);
            }
        }
        return redirect($this->prefix.'media')->with(['msg' => 'Delete Success', 'status' => 'success']);
    }

    /**
     * Display a listing of pages.
     * @return Response
     */
    public function pages()
    {
        $page_meta_title = 'Page';
        return view('blog::admin.pages')->with(['page_meta_title' => $page_meta_title]);
    }

    /**
     * Show single page.
     * @param $slug
     * @return Response
     */
    public function show_page($slug){
        $page_meta_title = 'Page';
        $page = Posts::where('slug', $slug)->first();
        if (isset($page)) {
            return view('blog::admin.single_page')->with(['page_meta_title' => $page_meta_title, 'page' => $page]);
        } else {
            return redirect(route('panel.page__index'))->with('msg', 'Page Not Found')->with('status', 'danger');
        }
    }

    /**
     * Get pages for datatable(ajax).
     * @param  Request $request
     * @return Response
     */
    public function ajaxPages(Request $request)
    {
        $order = $request->order[0];
        $col = $request->columns["{$order['column']}"]['data'] ?? 'created_at'; 
        $direction = $order['dir'] ?? 'desc';
        
        $query = Posts::where('post_type','page')->orderBy($col,$direction);
        $search = $request->search['value'];
        if (isset($search)) {
            $query = $query->where('title', 'like', '%'.$search.'%');   
        }
        $output['data'] = $query->offset($request['start'])->limit($request['length'])->get();

        foreach ($output['data'] as $key => $data) {
            $user = $data->user;
            if (!$user) {
                $user = new \stdClass;
                $user->username = 'admin';
            } 
            $data->author = $user;
        }

        $output['recordsTotal'] = $query->count();
        $output['recordsFiltered'] = $output['recordsTotal'];
        $output['draw'] = intval($request->input('draw'));
        $output['length'] = 10;

        return $output;
    }

    /**
     * Show the form for creating a new page.
     * @return Response
     */
    public function addPage()
    {
        $page_meta_title = 'Page';
        $templates = PostHelper::get_page_templates_list();

        return view('blog::admin.page_add')->with(['page_meta_title' => $page_meta_title, 'templates' => $templates]);
    }

    /**
     * Store a newly created page in storage.
     * @param  Request $request
     * @return Response
     */
    public function addPagePost(Request $request)
    {
        $this->validate($request, [
            'title' => 'required'
        ], PostHelper::validation_messages());

        $title = $request->input('title');
        $slug = PostHelper::make_slug($title);
        $body = $request->input('content') ?? '';
        $featured_img = $request->input('featured_image');
        $status = $request->get('status');
        $published_at = $request->input('published_at');
        $meta_title = $request->input('meta_title');
        $meta_desc = $request->input('meta_desc');
        $meta_keyword = $request->input('meta_keyword');
        $page_template = $request->get('page_template');

        if ($published_at == 'immediately') {
            $published_at = Carbon::now()->toDateTimeString();
        }

        $slug_check = Posts::withTrashed()->where('slug', $slug)->first();
        if (isset($slug_check)) {
            $slug = $slug.'-'.date('s');
        }

        DB::beginTransaction();
        try {
            $store = new Posts;
            $store->slug_id = PostHelper::getSlugID();
            $store->title = $title;
            $store->slug = $slug;
            $store->post_type = 'page';
            $store->content = $body;
            $store->featured_image = $featured_img;
            $store->author = Auth::user()->id;
            $store->status = $status;
            $store->published_at = $published_at;
            $store->save();

            $meta_contents = array();
            $meta_contents[] = ['post_id' => $store->id, 'meta_key' => 'meta_title', 'meta_value' => $meta_title];
            $meta_contents[] = ['post_id' => $store->id, 'meta_key' => 'meta_desc', 'meta_value' => $meta_desc];
            $meta_contents[] = ['post_id' => $store->id, 'meta_key' => 'meta_keyword', 'meta_value' => $meta_keyword];
            $meta_contents[] = ['post_id' => $store->id, 'meta_key' => 'page_template', 'meta_value' => $page_template];

            PostMeta::insert($meta_contents);

            DB::commit();
            return redirect(route('panel.page__view', $store->id))->with(['msg' => 'Saved', 'status' => 'success']);
        } catch (\Exception $e) {
            DB::rollback();
            if (config('app.debug')) {
                return redirect(route('panel.page__add'))->with(['msg' => 'Save Error'.$e, 'status' => 'danger'])->withInput();
            }
            return redirect(route('panel.page__add'))->with(['msg' => 'Save Error', 'status' => 'danger'])->withInput();
        }
    }

    /**
     * Show the form for editing the specified page.
     * @param $id
     * @return Response
     */
    public function viewPage($id)
    {
        $page = Posts::where('id', $id)->first();
        if (isset($page)) {
            $page_meta = Posts::find($id)->metas->pluck('meta_value', 'meta_key')->all();
            $templates = PostHelper::get_page_templates_list();

            return view('blog::admin.page_edit')->with([
                        'page_meta_title' => 'Page',
                        'page' => $page,
                        'page_meta' => (object)$page_meta,
                        'templates' => $templates
                    ]);
        } else {
            return redirect(route('panel.page__index'))->with(['msg' => 'Page Not Found', 'status' => 'danger']);
        }
    }

    /**
     * Update the specified page in storage.
     * @param  Request $request, $id
     * @return Response
     */
    public function updatePage(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required'
        ], PostHelper::validation_messages());

        DB::beginTransaction();
        try {
            $update = Posts::where('id', $id)->first();
            $update->title = $request->input('title');
            $update->content = $request->input('content') ?? '';
            $update->featured_image = $request->input('featured_image');
            $update->status = $request->input('status');
            $update->published_at = Carbon::parse($request->input('published_at'))->toDateTimeString();
            $update->save();

            PostMeta::where('post_id',$update->id)->delete();
            $meta_contents = array();
            $meta_contents[] = ['post_id' => $update->id, 'meta_key' => 'meta_title', 'meta_value' => $request->input('meta_title')];
            $meta_contents[] = ['post_id' => $update->id, 'meta_key' => 'meta_desc', 'meta_value' => $request->input('meta_desc')];
            $meta_contents[] = ['post_id' => $update->id, 'meta_key' => 'meta_keyword', 'meta_value' => $request->input('meta_keyword')];
            $meta_contents[] = ['post_id' => $update->id, 'meta_key' => 'page_template', 'meta_value' => $request->input('page_template')];

            PostMeta::insert($meta_contents);

            DB::commit();
            return redirect(route('panel.page__view', $update->id))->with(['msg' => 'Saved', 'status' => 'success']);
        } catch (\Exception $e) {
            DB::rollback();
            if (config('app.debug')) {
                return redirect(route('panel.page__view',$id))->with(['msg' => 'Save Error '.$e, 'status' => 'danger']);
            }
            return redirect(route('panel.page__view',$id))->with(['msg' => 'Save Error', 'status' => 'danger']);
        }
    }
    
    /**
     * Move page to trash.
     * @param $id
     * @return Response
     */
    public function removePage($id)
    {
        $delete = Posts::find($id);
        if ($delete){
            if ($delete->delete()) {
                return redirect(route('panel.page__index'))->with(['msg' => 'Deleted', 'status' => 'success']);
            }
        }
        return redirect(route('panel.page__index'))->with(['msg' => 'Delete error', 'status' => 'danger']);
    }

    /**
     * Move multiple page to trash.
     * @param  Request $request
     * @return Response
     */
    public function massdeletePage(Request $request)
    {
        $id = json_decode($request->id);
        foreach ($id as $id) {
            $delete = Posts::find($id);
            if ($delete) {
                if (!$delete->delete()) {
                    return redirect(route('panel.page__index'))->with(['msg' => 'Delete Error', 'status' => 'danger']);
                }
            } else {
                return redirect(route('panel.page__index'))->with(['msg' => 'Delete Error. Page Not Found', 'status' => 'danger']);
            }
        }
        return redirect(route('panel.page__index'))->with(['msg' => 'Delete Success', 'status' => 'success']);
    }
    // end page controller

    // trash controller
    /**
     * Display list of deleted post.
     * @param  Request $request
     * @return Response
     */
    public function trash()
    {
        $page_meta_title = 'Trash';
        return view('blog::admin.trash')->with(['page_meta_title' => $page_meta_title]);
    }

    /**
     * Get posts for datatable(ajax).
     * @param  Request $request
     * @return Response
     */
    public function ajaxtrashPosts(Request $request)
    {
        $order = $request->order[0];
        $col = $request->columns["{$order['column']}"]['data'] ?? 'created_at'; 
        $direction = $order['dir'] ?? 'desc';
        
        $query = Posts::onlyTrashed()->orderBy($col,$direction);
        $search = $request->search['value'];
        if (isset($search)) {
            $query = $query->where('title', 'like', '%'.$search.'%');   
        }
        $output['data'] = $query->offset($request['start'])->limit($request['length'])->get();

        foreach ($output['data'] as $key => $data) {
            $user = $data->user;
            if (!$user) {
                $user = new \stdClass;
                $user->username = 'admin';
            } 
            $data->author = $user;
        }
        
        $output['recordsTotal'] = $query->count();
        $output['recordsFiltered'] = $output['recordsTotal'];
        $output['draw'] = intval($request->input('draw'));
        $output['length'] = 10;

        return $output;
    }

     /**
     * Remove permanent multiple page from storage.
     * @param  Request $request
     * @return Response
     */
    public function deleteTrash($id)
    {
        $delete = Posts::onlyTrashed()->find($id);
        if ($delete){  
            $delete->forceDelete();  
            return redirect(route('panel.post.trash__index'))->with(['msg' => 'Deleted', 'status' => 'success']);
        }
        return redirect(route('panel.post.trash__index'))->with(['msg' => 'Delete error', 'status' => 'danger']);
    }

     /**
     * Restore multiple page from storage.
     * @param  Request $request
     * @return Response
     */
    public function restoreTrash($id)
    {
        $post = Posts::onlyTrashed()->find($id);
        if ($post){  
            $post->restore();  
            return redirect(route('panel.post.trash__index'))->with(['msg' => 'Restored', 'status' => 'success']);
        }
        return redirect(route('panel.post.trash__index'))->with(['msg' => 'Restore Error', 'status' => 'danger']);
    }

     /**
     * Remove permanent multiple post from storage.
     * @param  Request $request
     * @return Response
     */
    public function massdeleteTrash(Request $request)
    {
        $id = json_decode($request->id);
        foreach ($id as $id) {
            $delete = Posts::onlyTrashed()->find($id);
            if ($delete) {
                if (!$delete->forceDelete()) {
                    return redirect(route('panel.post.trash__index'))->with(['msg' => 'Delete Error', 'status' => 'danger']);
                }
            } else {
                return redirect(route('panel.post.trash__index'))->with(['msg' => 'Delete Error. Page Not Found', 'status' => 'danger']);
            }
        }
        return redirect(route('panel.post.trash__index'))->with(['msg' => 'Delete Success', 'status' => 'success']);
    }

     /**
     * Restore multiple post from storage.
     * @param  Request $request
     * @return Response
     */
    public function massrestoreTrash(Request $request)
    {
        $id = json_decode($request->id);
        foreach ($id as $id) {
            $delete = Posts::onlyTrashed()->find($id);
            if ($delete) {
                if (!$delete->restore()) {
                    return redirect(route('panel.post.trash__index'))->with(['msg' => 'Restore Error', 'status' => 'danger']);
                }
            } else {
                return redirect(route('panel.post.trash__index'))->with(['msg' => 'Restore Error. Page Not Found', 'status' => 'danger']);
            }
        }
        return redirect(route('panel.post.trash__index'))->with(['msg' => 'Restore success', 'status' => 'success']);
    }

    /**
     * Empty trash.
     * @return Response
     */
    public function emptyTrash()
    {
        try{
            Posts::onlyTrashed()->forceDelete();
            return redirect(route('panel.post.trash__index'))->with(['msg' => 'Empty trash success', 'status' => 'success']);
        } catch (\Exception $e) {
            if (config('app.debug')) {
                return redirect(route('panel.post.trash__index'))->with(['msg' => 'Something went wrong : '.$e, 'status' => 'danger']);
            }
            return redirect(route('panel.post.trash__index'))->with(['msg' => 'Something went wrong', 'status' => 'danger']);
        }
    }
    // end trash controller

    /**
     * Get all category parent to select category parent.
     * @param  $category_id
     * @return Response
     */
    public static function get_category_parent($category_id = ''){
         return PostHelper::get_category_parent($category_id);
    }

    /**
     * Get all category for list on post form.
     * @param  $post_id
     * @return Response
     */
    public static function get_all_category($post_id = 0){
        return PostHelper::get_all_category($post_id);
    }
    
    /**
     * Show site setting form.
     * @return Response
     */
    public function site_setting_view(){
        $page_meta_title = 'Site Setting';
        $option = Option::pluck('meta_value', 'meta_key')->all();

        return view('blog::admin.setting')->with([  'page_meta_title' => $page_meta_title,
                                                    'option' => (object)$option, 
                                                ]);
    }

    /**
     * Save site setting.
     * @param  $post_id
     * @return Response
     */
    public function site_setting_save(Request $request){
        $settings[] = ['name' => 'link_fb', 'value' => $request->input('link_fb')];
        $settings[] = ['name' => 'link_tw', 'value' => $request->input('link_tw')];
        $settings[] = ['name' => 'link_gplus', 'value' => $request->input('link_gplus')];
        $settings[] = ['name' => 'link_ig', 'value' => $request->input('link_ig')];
        $settings[] = ['name' => 'link_in', 'value' => $request->input('link_in')];
        $settings[] = ['name' => 'link_yt', 'value' => $request->input('link_yt')];
        $settings[] = ['name' => 'gtag_manager', 'value' => $request->input('gtag_manager')];
        $settings[] = ['name' => 'fb_pixel', 'value' => $request->input('fb_pixel')];
        $settings[] = ['name' => 'default_meta_title', 'value' => $request->input('default_meta_title')];
        $settings[] = ['name' => 'default_meta_desc', 'value' => $request->get('default_meta_desc')];
        $settings[] = ['name' => 'default_meta_keyword', 'value' => $request->get('default_meta_keyword')];

        try {
            for ($i=0; $i < count($settings) ; $i++) { 
                $save = Option::where('meta_key', $settings[$i]['name'])->first();
                if (isset($save)) {
                    $save->meta_value = $settings[$i]['value'];
                    $save->timestamps = false;
                    $save->save();
                } else {
                    Option::insert(['meta_key' => $settings[$i]['name'], 'meta_value' => $settings[$i]['value']]);
                }
            }

            return redirect(route('panel.setting.site__index'))->with(['msg' => 'Saved', 'status' => 'success']);
        } catch (\Exception $e) {
            if (config('app.debug')) {
                return redirect(route('panel.setting.site__index'))->with(['msg' => 'Save Error '.$e, 'status' => 'danger']);    
            }
            return redirect(route('panel.setting.site__index'))->with(['msg' => 'Save Error ', 'status' => 'danger']);    
        }
    }

    // slider controller
    /**
     * Display a listing of slider.
     * @return Response
     */
    public function head_slider(){
        $page_meta_title = 'Slider';
        $sliders = Slider::orderBy('created_at','desc')->get();
        return view('blog::admin.slider') -> with(['page_meta_title' => $page_meta_title,'sliders' => $sliders]);
    }

    /**
     * Show the form for creating slider.
     * @return Response
     */
    public function new_slider_view(){
        $page_meta_title = 'Slider';
        
        return view('blog::admin.slider-add') -> with(['page_meta_title' => $page_meta_title]);
    }

    /**
     * Store newly created slider in storage.
     * @param  Request $request
     * @return Response
     */
    public function new_slider_act(Request $req){
        $title = $req->input('title');
        $description = $req->input('description');
        $slider_img = $req->input('slider_img');
        $btn_text = $req->input('btn_text');
        $btn_link = $req->input('btn_link');

        $slider = new Slider();
        $slider->title = $title;
        $slider->description = $description;
        $slider->image = $slider_img;
        $slider->btn_text = $btn_text;
        $slider->link = $btn_link;
        if ($slider -> save()) {
            return redirect(route('panel.slider__view', $slider->id)) -> with("msg","Berhasil Ditambahkan")-> with("status","success");
        }else{
            return redirect(route('panel.slider__index')) -> with("msg","Gagal Ditambahkan")-> with("status","success");    
        }
    }

    /**
     * Show the form for editing the specified slider.
     * @param  $id
     * @return Response
     */
    public function edit_slider_view($id){
        $page_meta_title = 'Slider';
        $slider = Slider::where('id',$id)->first();

        if (isset($slider)) {

            return view('blog::admin.slider-edit') -> with(['page_meta_title' => $page_meta_title, 'slider' => $slider]);
        }else {
            return redirect(route('panel.slider__index'))->with(['msg' => 'Slider tidak ditemukan', 'status' => '4']);
        }
    }

    /**
     * Update specified slider in storage.
     * @param  Request $request, $id
     * @return Response
     */
    public function edit_slider_act(Request $req, $id){
        $title = $req->input('title');
        $description = $req->input('description');
        $slider_img = $req->input('slider_img');
        $btn_text = $req->input('btn_text');
        $btn_link = $req->input('btn_link');

        $slider = Slider::where('id',$id)->first();
        $slider->title = $title;
        $slider->description = $description;
        $slider->image = $slider_img;
        $slider->btn_text = $btn_text;
        $slider->link = $btn_link;
        if ($slider -> save()) {
            return redirect(route('panel.slider__view', $slider->id)) -> with("msg","Berhasil Menyimpan")-> with("status","success");
        }else{
            return redirect(route('panel.slider__view', $slider->id)) -> with("msg","Gagal mengedit data photo")-> with("status","danger");
        }
    }

    /**
     * Remove specified slider in storage.
     * @param  $id
     * @return Response
     */
    public function hapus_slider($id){
        $slider = Slider::where('id',$id)->first();
        if ($slider -> delete()) {
            return redirect(route('panel.slider__index')) -> with("msg","Slider berhasil dihapus")-> with("status","success");
        }else{
            return redirect(route('panel.slider__index')) -> with("msg","Gagal menghapus Foto")-> with("status","danger");
        }
    }
    // end slider controller

    // start keyword controller
    /**
     * Display a listing of keyword.
     * @return Response
     */
    public function keywords(){
        $page_meta_title = 'Keywords';
        return view('blog::admin.keywords')->with(['page_meta_title' => $page_meta_title]);
    }

    /**
     * Get keywords for datatable(ajax).
     * @param  Request $request
     * @return Response
     */
    public function ajaxKeywords(Request $request)
    {
        $order = $request->order[0];
        $col = $request->columns["{$order['column']}"]['data'] ?? 'created_at'; 
        $direction = $order['dir'] ?? 'desc';
        
        $query = Keywords::withoutGlobalScopes()->orderBy($col,$direction);
        $search = $request->search['value'];
        if (isset($search)) {
            $query = $query->where('title', 'like', '%'.$search.'%');   
        }
        $output['data'] = $query->offset($request['start'])
                                ->limit($request['length'])
                                ->get();

        $output['recordsTotal'] = $query->count();
        $output['recordsFiltered'] = $output['recordsTotal'];
        $output['draw'] = intval($request->input('draw'));
        $output['length'] = 10;

        return $output;
    }

    /**
     * Show the form for creating a new keyword.
     * @return Response
     */
    public function addKeyword()
    {
        $page_meta_title = 'Keywords';
        $tags = Tags::get();
        $categories = Categories::get();
        $pages = Posts::where('post_type', 'page')->get();

        return view('blog::admin.keywords_add')->with(['page_meta_title' => $page_meta_title,
                                                        'tags' => $tags,
                                                        'categories' => $categories,
                                                        'pages' => $pages
                                                    ]);
    }

    /**
     * Store a newly created keyword in storage.
     * @param  Request $request
     * @return Response
     */
    public function addKeywordPost(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'slug' => 'required|unique:keywords'
        ], PostHelper::validation_messages());

        $title = $request->input('title');
        $slug = $request->input('slug');
        $meta_title = $request->input('meta_title');
        $meta_desc = $request->input('meta_desc');
        $meta_keyword = $request->input('meta_keyword');
        $status = $request->input('status');
        $keyword_type = $request->input('keyword_type');

        $keyword_data['check'] = $request->input('data_check');
        $keyword_data['page'] = $request->input('data_page');
        $keyword_data['categories'] = $request->input('data_categories') ?? [];
        $keyword_data['tags'] = $request->input('data_tags') ?? [];
        $data = json_encode($keyword_data);

        DB::beginTransaction();
        try {
            $store = new Keywords;
            $store->slug = $slug;
            $store->title = $title;
            $store->meta_title = $meta_title;
            $store->meta_desc = $meta_desc;
            $store->meta_keyword = $meta_keyword;
            $store->status = $status;
            $store->keyword_type = $keyword_type;
            $store->data = $data;
            $store->save();
            
            DB::commit();
            return redirect(route('panel.keywords__view', $store->id))->with(['msg' => 'Saved', 'status' => 'success']);
        } catch (\Exception $e) {
            DB::rollback();
            if (config('app.debug')) {
                return redirect(route('panel.keywords__add'))->with(['msg' => 'Save Error : '.$e, 'status' => 'danger'])->withInput();
            } 
            return redirect(route('panel.keywords__add'))->with(['msg' => 'Save Error', 'status' => 'danger'])->withInput();
        }
    }

    /**
     * Show the form for editing keyword.
     * @param $id
     * @return Response
     */
    public function viewKeyword($id)
    {
        $page_meta_title = 'Keywords';
        $keyword = Keywords::find($id);
        if ($keyword) {
            $tags = Tags::get();
            $categories = Categories::get();
            $pages = Posts::where('post_type', 'page')->get();
            $item_id = $keyword->id;
            $keyword->data = json_decode($keyword->data);

            return view('blog::admin.keywords_edit')->with([ 'item_id' => $item_id, 
                                                         'page_meta_title' => $page_meta_title, 
                                                         'categories' => $categories,
                                                         'tags' => $tags,
                                                         'pages' => $pages,
                                                         'data' => $keyword , 
                                                        ]);
        } else {
            return redirect(route('panel.keywords__index'))->with(['msg' => 'Data Not Found', 'status' => 'danger']);
        }
    }

    /**
     * Update the specified keyword in storage.
     * @param  Request $request, $id
     * @return Response
     */
    public function updateKeyword(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required'
        ], PostHelper::validation_messages());

        $title = $request->input('title');
        $slug = $request->input('slug');
        $description = $request->input('description');
        $meta_title = $request->input('meta_title');
        $meta_desc = $request->input('meta_desc');
        $meta_keyword = $request->input('meta_keyword');
        $status = $request->input('status');
        $keyword_type = $request->input('keyword_type');

        $keyword_data['check'] = $request->input('data_check');
        $keyword_data['page'] = $request->input('data_page');
        $keyword_data['categories'] = $request->input('data_categories') ?? [];
        $keyword_data['tags'] = $request->input('data_tags') ?? [];
        $data = json_encode($keyword_data);

        DB::beginTransaction();
        try {
            $update = Keywords::where('id', $id)->first();

            $check_slug = Keywords::where('id', '!=', $id)->where('slug', $update->slug)->first();
            if ($check_slug) {
                return redirect(route('panel.keywords__view', $id))->with(['msg' => 'Slug must be unique', 'status' => 'danger']);
            }

            $update->slug = $slug;
            $update->title = $title;
            $update->meta_title = $meta_title;
            $update->meta_desc = $meta_desc;
            $update->meta_keyword = $meta_keyword;
            $update->status = $status;
            $update->keyword_type = $keyword_type;
            $update->data = $data;
            $update->save();

            DB::commit();
            return redirect(route('panel.keywords__view', $update->id))->with(['msg' => 'Saved', 'status' => 'success']);
        } catch (\Exception $e) {
            DB::rollback();
            if (config('app.debug')) {
                return redirect(route('panel.keywords__view', $id))->with(['msg' => 'Save error : '.$e, 'status' => 'danger']);
            }

            return redirect(route('panel.keywords__view', $id))->with(['msg' => 'Save error', 'status' => 'danger']);
        }
    }

    /**
     * Remove specific keyword from storage.
     * @param $id
     * @return Response
     */
    public function removeKeyword($id)
    {
        $delete = Keywords::find($id);
        if ($delete){
            $delete->delete();
            return redirect(route('panel.keywords__index'))->with(['msg' => 'Deleted', 'status' => 'success']);
        }
        return redirect(route('panel.keywords__index'))->with(['msg' => 'Delete error', 'status' => 'danger']);
    }

    /**
     * Remove multiple keyword from storage.
     * @param  Request $request
     * @return Response
     */
    public function massdeleteKeywords(Request $request)
    {
        $id = json_decode($request->id);
        foreach ($id as $id) {
            $delete = Keywords::find($id);
            if ($delete) {
                if (!$delete->delete()) {
                    return redirect(route('panel.keywords__index'))->with(['msg' => 'Delete Error', 'status' => 'danger']);
                }
            } else {
                return redirect(route('panel.keywords__index'))->with(['msg' => 'Delete Error. Post Not Found', 'status' => 'danger']);
            }
        }
        return redirect(route('panel.keywords__index'))->with(['msg' => 'Delete Success', 'status' => 'success']);
    }
    // end keyword controller

    // user controller
    /**
     * Display a listing of users.
     * @return Response
     */
    public function users(){
        $page_meta_title = 'Users';
        return view('blog::admin.users')->with(['page_meta_title' => $page_meta_title]);
    }

    /**
     * Get users for datatable(ajax).
     * @param  Request $request
     * @return Response
     */
    public function ajaxUsers(Request $request)
    {
        $order = $request->order[0];
        $col = $request->columns["{$order['column']}"]['data'] ?? 'created_at'; 
        $direction = $order['dir'] ?? 'desc';
        
        $query = User::orderBy($col,$direction);
        $search = $request->search['value'];
        if (isset($search)) {
            $query = $query->where('username', 'like', "%$search%")
                            ->orWhere('name', 'like', "%$search%");   
        }
        $output['data'] = $query->offset($request['start'])
                                ->limit($request['length'])
                                ->get();

        $output['recordsTotal'] = $query->count();
        $output['recordsFiltered'] = $output['recordsTotal'];
        $output['draw'] = intval($request->input('draw'));
        $output['length'] = 10;

        return $output;
    }

    /**
     * Show the form for creating a new user.
     * @return Response
     */
    public function addUser()
    {
        $page_meta_title = 'Users';
        return view('blog::admin.users_add')->with(['page_meta_title' => $page_meta_title]);
    }

    /**
     * Store a newly created user in storage.
     * @param  Request $request
     * @return Response
     */
    public function addUserPost(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'username' => 'required|string|max:255|unique:users',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'role' => 'required|string'
        ], PostHelper::validation_messages());

        $store = new User;
        $store->name = $request->input('name');
        $store->username = $request->input('username');
        $store->email = $request->input('email');
        $store->password = bcrypt($request->input('password'));
        $store->role = $request->input('role');
        if ($store->save()) {
            return redirect(route('panel.users__view', $store->id))->with(['msg' => 'Saved', 'status' => 'success']);
        }
        return redirect(route('panel.users__add'))->with(['msg' => 'Save Error', 'status' => 'danger'])->withInput();
            
    }

    /**
     * Show the form for editing user.
     * @param $id
     * @return Response
     */
    public function viewUser($id)
    {
        $page_meta_title = 'Users';
        $data = User::find($id);
        if ($data) {
            return view('blog::admin.users_edit')->with([ 'page_meta_title' => $page_meta_title, 
                                                         'data' => $data, 
                                                        ]);
        } else {
            return redirect(route('panel.users__index'))->with(['msg' => 'Data Not Found', 'status' => 'danger']);
        }
    }

    /**
     * Update the specified user in storage.
     * @param  Request $request, $id
     * @return Response
     */
    public function updateUser(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,email,'.$id
        ], PostHelper::validation_messages());


        $user = User::find($id);
        if (!$user) {
            return redirect(route('panel.users__index'))->with(['msg' => 'User not found', 'status' => 'danger']);
        }
        $user->name = $request->input('name');
        $user->email = $request->input('email');

        // change password?
        if ($request->has('change_password')) {
            $this->validate($request, [
                'password' => 'required|string|min:6|confirmed'
            ], PostHelper::validation_messages());

            $user->password = bcrypt($request->input('password'));
        }
        if ($request->has('role')) {
            $user->role = $request->input('role');
        }
        if ($user->save()) {
            return redirect(route('panel.users__view', $user->id))->with(['msg' => 'Saved', 'status' => 'success']);
        }

        return redirect(route('panel.users__view', $id))->with(['msg' => 'Something went wrong', 'status' => 'danger']);
    }

    /**
     * Remove specific user from storage.
     * @param $id
     * @return Response
     */
    public function removeUser($id)
    {
        $delete = User::find($id);
        if ($delete){
            $delete->delete();
            return redirect(route('panel.users__index'))->with(['msg' => 'Deleted', 'status' => 'success']);
        }
        return redirect(route('panel.users__index'))->with(['msg' => 'Delete error', 'status' => 'danger']);
    }
    // end user controller
}
