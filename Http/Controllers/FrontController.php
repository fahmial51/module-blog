<?php

namespace Modules\Blog\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller as BaseController;
use App\Http\Controllers\Controller;
use Modules\Blog\Entities\Posts;
use Modules\Blog\Entities\Categories;
use Modules\Blog\Entities\Tags;
use Modules\Blog\Entities\PostCategories;
use Modules\Blog\Entities\PostTags;
use Modules\Blog\Entities\Media;
use Modules\Blog\Entities\Comments;
use Modules\Blog\Entities\Keywords;
use Modules\Blog\Http\Helpers\PostHelper;
use Carbon\Carbon;
use Shortcode;

class FrontController extends Controller{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    
    public function __construct(){
      Shortcode::enable();
    }

    // post controller
    public function index(){
    	$page_title = 'Home';
      return view('blog::pages.blog.index')->with(['page_title' => $page_title]);
    }

    /* show single post */
    public function single_post($slug){
      $slug_key = substr($slug, 0, -5);
      $slug_id = substr($slug, -4);

      $post = Posts::where('slug_id', $slug_id)->first();
      if ($post) {
        if ($slug_key == $post->slug) {
          $post_meta = $post->metas->pluck('meta_value', 'meta_key')->all();
        } else if ($slug_key && $slug_key != '') {
          $slug_key = PostHelper::unfilterKeyword($slug_key, $post->slug);
          $meta = Keywords::where('slug', $slug_key)->keywordType('single')->first();
          // use single keyword?
          if ($meta) {
            $post_meta = $meta;
          } else {
            return redirect()->route('blog::pages.blog.single', $post->full_slug);
            exit;
          }
        } else {
            return redirect()->route('blog::pages.blog.single', $post->full_slug);
        }

        $post_meta['meta_title'] = PostHelper::filterKeyword($post_meta['meta_title'], $post->title);
        $post_meta['meta_desc'] = PostHelper::filterKeyword($post_meta['meta_desc'], $post->title);
        $post_meta['meta_keyword'] = PostHelper::filterKeyword($post_meta['meta_keyword'], $post->title);

        $Meta = app()->Meta;
        $Meta->set('meta_title', ucwords($post_meta['meta_title'] ?? $post->title));
        $Meta->set('meta_desc', $post_meta['meta_desc'] ?? str_limit(html_entity_decode(strip_tags($post->content)), 250));
        $Meta->set('meta_keyword', $post_meta['meta_keyword'] ?? '');

        $post_tags = $post->tags->all();        
        $post_categories = $post->categories->all();    
        $post_comments = $post->comments->where('parent', null)->all();        
        foreach ($post_comments as $key => $data) {
            $data->user = Comments::find($data->user_id)->user;
            $data->children = Comments::find($data->id)->children;
        }
        $post->author = $post->user;  

        return view('blog::pages.blog.single')->with(['post_meta' => $post_meta, 
                                                      'post_categories' => $post_categories, 
                                                      'post_tags' => $post_tags, 
                                                      'post_comments' => $post_comments,
                                                      'post' => $post
                                                      ])
                                              ->withShortcodes();
      } else {
          return redirect('/')->with('msg', 'Post Not Found')->with('status', 'danger');
      }
    }

    /* show single post */
    public function single_page($slug) {
      $slug_key = substr($slug, 0, -5);
      $slug_id = substr($slug, -4);
      $output_type = '';
      $Meta = app()->Meta;

      $page = Posts::postType('page')->where('slug_id', $slug_id)->first();

      if ($page) {
        if ($slug_key == $page->slug) {
          $page_meta = $page->metas->pluck('meta_value', 'meta_key')->all();
        } else if (!empty($slug_key)) {
          $slug_key = PostHelper::unfilterKeyword($slug_key, $page->slug);
          $meta = Keywords::where('slug', $slug_key)->keywordType('single')->first();
          // use single keyword?
          if ($meta) {
            $page_meta = $meta;
          } else {
            return redirect()->route('single.page', $page->full_slug);
            exit;
          }
        } else {
            return redirect()->route('single.page', $page->full_slug);
        }

        $page->author = $page->user; 
        $page_template = $page->metas->where('meta_key', 'page_template')->pluck('meta_value', 'meta_key')['page_template'] ?? '';
        $page_meta['page_template'] = $page_template;
        $page_meta['meta_title'] = PostHelper::filterKeyword($page_meta['meta_title'], $page->title);
        $page_meta['meta_desc'] = PostHelper::filterKeyword($page_meta['meta_desc'], $page->title);
        $page_meta['meta_keyword'] = PostHelper::filterKeyword($page_meta['meta_keyword'], $page->title);

        $Meta->set('meta_title', ucwords($page_meta['meta_title'] ?? $page->title));
        $Meta->set('meta_desc', $page_meta['meta_desc'] ?? str_limit(html_entity_decode(strip_tags($page->content)), 250));
        $Meta->set('meta_keyword', $page_meta['meta_keyword'] ?? '');

        // return view page
        $output_type = 'single-page';
      } else {
        $keyword_global = Keywords::keywordType('global')->where('slug', $slug)->first();
        if ($keyword_global) {
          $keyword_global->data = json_decode($keyword_global->data);

          // return view global keyword
          if ($keyword_global->data->check == 1) {
            $page = Posts::postType('page')->findOrFail($keyword_global->data->page);
            $page_meta = $page->metas->pluck('meta_value', 'meta_key')->all();
            
            $page_meta['meta_title'] = PostHelper::filterKeyword($page_meta['meta_title'], $page->title);
            $page_meta['meta_desc'] = PostHelper::filterKeyword($page_meta['meta_desc'], $page->title);
            $page_meta['meta_keyword'] = PostHelper::filterKeyword($page_meta['meta_keyword'], $page->title);

            $Meta->set('meta_title', ucwords($keyword_global->meta_title ?? $page_meta['meta_title'] ?? $page->title));
            $Meta->set('meta_desc',  $keyword_global->meta_desc ?? $page_meta['meta_desc'] ?? str_limit(html_entity_decode(strip_tags($page->content)), 250));
            $Meta->set('meta_keyword', $keyword_global->meta_keyword ?? $page_meta['meta_keyword'] ?? '');

            // return view page
            $output_type = 'single-page';
          } else {
            $post_categories = PostCategories::whereIn('category_id', $keyword_global->data->categories)->pluck('post_id')->toArray();
            $post_tags = PostTags::whereIn('tag_id', $keyword_global->data->tags)->pluck('post_id')->toArray();
            $post_id = array_merge($post_categories, $post_tags);
            $posts = Posts::whereIn('id', $post_id)->paginate(8);
            
            $Meta->set('meta_title', ucwords($keyword_global->meta_title ?? $keyword_global->title));
            $Meta->set('meta_desc',  $keyword_global->meta_desc ?? '');
            $Meta->set('meta_keyword', $keyword_global->meta_keyword ?? '');
            
            // return view list post
            $output_type = 'list-posts';
          }

        }
      }
      switch ($output_type) {
          case 'single-page':
              if (!empty($page_meta['page_template'])) {
                $page_template = str_replace('template.', '', $page_meta['page_template']);
                $dir = resource_path("views/template/$page_template.blade.php");
                if (file_exists($dir)) {
                    return view($page_meta['page_template'])->with(['page' => $page]);
                }
              }
              dd('single-page', $page);
              return view('single-page')->with(['page' => $page]);
              break;

          case 'list-posts':
              dd('list-post', $posts);
              return view('list-post')->with(['posts' => $posts]);
              break;
          
          default:
              return redirect('/')->with('msg', 'Page Not Found')->with('status', 'danger');
              break;
      }
      
    }

    /* show list post by category */
    public function postsCategory($category_slug) {
      $cat = Categories::where('slug', $category_slug)->first();
      if ($cat) {
        $posts_id = PostCategories::where('category_id', $cat->id)->pluck('post_id')->toArray();
        $posts = Posts::whereIn('id', $posts_id)->paginate(8);

        $Meta = app()->Meta;
        $Meta->set('meta_title', ucwords($cat->name));
        $Meta->set('meta_desc', $cat->description);

        dd('list-post', $posts);
        return view('list-post')->with(['posts' => $posts]);
      }
      return redirect()->route('home');
    }

    /* show list post by tag */
    public function postsTag($tag_slug) {
      $tag = Tags::where('slug', $tag_slug)->first();
      if ($tag) {
        $posts_id = PostTags::where('tag_id', $tag->id)->pluck('post_id')->toArray();
        $posts = Posts::whereIn('id', $posts_id)->paginate(8);

        $Meta = app()->Meta;
        $Meta->set('meta_title', ucwords($tag->name));
        $Meta->set('meta_desc', $tag->description);
        dd('list-posts', $posts);
        return view('list-post')->with(['posts' => $posts]);
      }
      return redirect()->route('home');
    }

}
