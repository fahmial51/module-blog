//SWIPER SLIDER
var swiper = new Swiper('.swiper-container', {
  pagination: '.swiper-pagination',
  paginationClickable: true,
  nextButton: '.swiper-button-next',
  prevButton: '.swiper-button-prev',
  parallax: true,
  speed: 600,
});
//END OF SWIPER SLIDERs

//SMOOTHSCROLL
$(document).ready(function(){
  // Add smooth scrolling to all links
  $("a").on('click', function(event) {
    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function(){

        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
});
// END SMOOTHSCROLL


$( document ).ready(function() {
  $(".main-comment").click(function(){
    $( ".sublvlform" ).remove();
  });
  // $( ".comment-action span" ).click(function() {
  //   $( ".sublvlform" ).remove();
  //   var token = $('#token').val();
  //   var formAction = $("#formaction").attr('action');
  //   var id = $(this).closest('.comment-action').attr('id');;
  //     $('<form/>', {
  //             'id':'form'+id,
  //             'class':'sublvlform',
  //     }).appendTo('#'+id);
  //     $("#form"+id).dform({
  //         "action" : formAction,
  //         "method" : "post",
  //         "html" :
  //         [
  //             {
  //                 "name" : "comment",
  //                 "id": "comment-input2",
  //                 "type" : "textarea",
  //                 "class" : "comment-sub",
  //                 "placeholder" : "Comment disini"
  //             },
  //             {
  //               "name" : "_token",
  //               "type" : "hidden",
  //               "value" : token
  //             },
  //             {
  //               "name" : "commentid",
  //               "type" : "hidden",
  //               "value" : id
  //             },
  //             {
  //                 "type" : "submit",
  //                 "value" : "Send",
  //                   "class" : "submit-comment"
  //             }
  //         ]
  //     });
  //     $('.comment-sub').focus();
  // });
  //autoresize
  autosize(document.getElementById("comment-input"));
  autosize(document.getElementById("comment-input2"));
  //end autoresize
});

function loadComment(){
  var postid = $('input[name=postid]').val();
  $.ajax({
    type : 'get',
    url : '/blogs/getcomment/'+postid,
    success : function(data){
      $('#commentation').html(data);
    },
    error : function(err){
      console.log(error);
    }
  });
}
$('#commentation').ready(loadComment());

$(document).on('click', '.comment-action span', function() {
  $( ".sublvlform" ).remove();
  var parent = $(this).parent().parent();
  var id = $(this).parent().attr('id');
  parent.append('<div class="comment-action sublvlform"><form id="form'+id+'" action="" method="post"> <input type="hidden" name="commentid" value="'+id+'"><textarea placeholder="Comment disini" id="comment-input2" class="comment-sub" name="comment"></textarea> <button class="submit-comment">Send</button></form></div>');
});

$(document).on('click', '.submit-comment', function(e) {
  e.preventDefault();
  var id = $(this).parent().attr('id');
  var postId = $('input[name="postid"]').val();
  var data = new FormData($("#"+id)[0]);
  $.ajax({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      type: "POST",
      url: "/blogs/go-comment/"+postId,
      processData: false,
      contentType: false,
      data: data,
      success: function(msg){
        loadComment();
        console.log('msg');
      },
      error: function(err){
        console.log('err');
      }
  });
});
