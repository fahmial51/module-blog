@extends('blog::layouts.front')
@section('content')
<article class="post">
    <h1>{{ $post->title }}</h1>
    <span class="date">{{ $post->published_at }}</span>
    @if (Auth::check())
    <span class="edit"><a href="{{ url('/admin/blog/post/'.$post->id.'/view') }}">EDIT</a></span>
    @endif
    <div class="fimg-single-post">
      <img src="{{ $post->featured_image }}" width="800">
    </div>
    <div class="content-read">
        {!! $post->content !!}
    </div>
    <hr>
    <div class="author">
      <span>Author</span>
      <span class="author-name">{{ $post->author->name ?? 'admin' }}</span>
    </div>
    <div class="post-tags">
      <span>Post Tag</span>
      <ul>
        @foreach($post_tags as $tag)
        <li><a href="{{ url('/tag/'.$tag->slug) }}">{{ $tag->name }}</a></li>
        @endforeach
      </ul>
    </div>
    <div class="post-tags">
      <span>Post Category</span>
      <ul>
        @foreach($post_categories as $category)
        <li><a href="{{ url('/category/'.$category->slug) }}">{{ $category->name }}</a></li>
        @endforeach
      </ul>
    </div>
</article>

<div class="post-comments">
  <h3>Comments</h3>
  <ul>
    @foreach ($post_comments as $comment)
    <li class="comment-1">
      <div class="comment-image">
        <img src="{{asset('public/img/foto-komen/Finn.jpg')}}" alt="">
      </div>
      <div class="comment-content">
        <span class="comment-name">{{ $comment->user->name ?? 'Anonymus' }}</span>
        <div class="comment-value">
          <p>{{ $comment->content }}</p>
        </div>
      </div>
      @if (sizeof($comment->children) > 0)
      <ul>
        @foreach($comment->children as $children)
        <li class="comment-2">
          <div class="comment-image">
            <img src="{{asset('public/img/foto-komen/Finn.jpg')}}" alt="">
          </div>
          <div class="comment-content">
            <span class="comment-name">{{ $children->user->name }}</span>
            <div class="comment-value">
              <p>{{ $children->content }}</p>
            </div>
          </div>
          @if (sizeof($children->children) > 0)
          <ul>
            @foreach($children->children as $lastchild)
            <li class="comment-2">
              <div class="comment-image">
                <img src="{{asset('public/img/foto-komen/Finn.jpg')}}" alt="">
              </div>
              <div class="comment-content">
                <span class="comment-name">{{ $lastchild->user->name }}</span>
                <div class="comment-value">
                  <p>{{ $lastchild->content }}</p>
                </div>
              </div>
            </li>
            @endforeach
          </ul>
          @endif
        </li>
        @endforeach
      </ul>
      @endif
    </li>
    @endforeach
  </ul>
</div>

<h3>Keyword Single</h3>
<ul>
  @foreach(app()->Meta->get('keywords_single') as $single)
  <li>
    <a href="{{ route('single.post', PostHelper::filterKeyword($single->slug, $post->slug).'-'.$post->slug_id) }}">
      {{ PostHelper::filterKeyword($single->title, $post->title) }}
    </a>
  </li>
  @endforeach
</ul>


<h3>Keyword Global</h3>
<ul>
  @foreach(app()->Meta->get('keywords_global') as $global)
  <li>
    <a href="{{ route('single.page', $global->slug) }}">
      {{ $global->title }}
    </a>
  </li>
  @endforeach
</ul>

@stop
