@extends('blog::layouts.master')

@section('content')

<div class="col-md-12">
    @if ($errors->any())
    <div class="alert alert-danger alert-dismissable ">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        There is some error. Please check again
    </div>
    @endif

    <form id="keyword-form" method="post" action="{{ route('panel.keywords__update', $data->id) }}" accept-charset="UTF-8">
        <div class="row">         
            <div class="col-md-9"> 
                <h4 class="title">Edit Keyword</h4>
            </div>
            <div class="col-md-9 col-sm-6 col-xs-6"> 
                <a href="{{ route('panel.keywords__add') }}" class="btn btn-round btn-fill btn-info">
                    New Keyword +<div class="ripple-container"></div>
                </a>
                <a onclick="return confirm('Delete Keyword?');" href="{{route('panel.keywords__delete',$data->id)}}" class="btn btn-round btn-fill btn-danger">
                    Delete Keyword<div class="ripple-container"></div>
                </a>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-6">
                <button type="submit" class="btn btn-success pull-right">Save Keyword</button>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
            </div>
        </div>
        
        <div class="row" style="margin-top: 15px;">
            <div class="col-md-9">
                <div class="form-group">
                    <label class="control-label">Title</label>
                    @if ($errors->has('title'))
                    <div class="has-error">
                        <span class="help-block">
                            <strong>{{ $errors->first('title') }}</strong>
                        </span>
                    </div>
                    @endif
                    <input class="form-control post-title" type="text" name="title" value="{{ $data->title }}" placeholder="Enter Title Here" required="required">
                </div>

                <div class="form-group">
                    <label class="control-label">Slug</label>
                    @if ($errors->has('slug'))
                    <div class="has-error">
                        <span class="help-block">
                            <strong>{{ $errors->first('slug') }}</strong>
                        </span>
                    </div>
                    @endif
                    <input class="form-control post-slug" type="text" name="slug" value="{{ $data->slug }}" placeholder="keyword-slug" required="required">
                </div>

                <div class="panel panel-default" id="global-keyword" style="display: {{ $data->keyword_type == 'global' ? '' : 'none' }};">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                          Content <a data-toggle="collapse" href="#keyword-content"><i style="float: right;" class="fa fa-caret-down" aria-hidden="true"></i></a>
                        </h4>
                    </div>
                    <div id="keyword-content" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <label for="keyword_data_check">Use Page</label> 
                            <label class="switch">
                              <input type="checkbox" name="data_check" value="1" id="keyword_data_check"  {{ $data->data->check == 1 ? 'checked':'' }}>
                              <span class="slider round"></span>
                            </label>

                            <div class="form-group keyword_data_page">
                                <label for="data_page">Page</label>
                                <select class="myselect2 form-control" name="data_page" id="data_page">
                                    <option value="">Select page</option>
                                    @foreach($pages as $page)
                                    <option value="{{$page->id}}" {{ $data->data->page == $page->id ? 'selected':'' }}>{{$page->title}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group keyword_data_other">
                                <label for="data_categories">Category</label>
                                <select class="myselect2 form-control" name="data_categories[]" id="data_categories" multiple>
                                    <option value="">Select Category</option>
                                    @foreach($categories as $category)
                                    <option value="{{$category->id}}" {{ in_array($category->id, $data->data->categories) ? 'selected':'' }}>{{$category->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group keyword_data_other">
                                <label for="data_tags">Tag</label>
                                <select class="myselect2 form-control" name="data_tags[]" id="data_tags" multiple>
                                    <option value="">Select Tag</option>
                                    @foreach($tags as $tag)
                                    <option value="{{$tag->id}}" {{ in_array("$tag->id", $data->data->tags) ? 'selected':'' }}>{{$tag->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
               
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                          SEO Setting <a data-toggle="collapse" href="#post-seo"><i style="float: right;" class="fa fa-caret-down" aria-hidden="true"></i></a>
                        </h4>
                    </div>
                    <div id="post-seo" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <div class="form-group">
                                <label class="control-label">Meta Title</label>
                                <input value="{{ $data->meta_title }}" class="form-control" type="text" name="meta_title" maxlength="191">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Meta Deskripsi</label>
                                <textarea class="form-control" id="inputan" name="meta_desc" style="min-height: 100px;" maxlength="300">{{ $data->meta_desc }}</textarea>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Keyword</label>
                                <input value="{{ $data->meta_keyword }}" class="form-control" type="text" name="meta_keyword" maxlength="191">
                                <small>Contoh : keyword 1, keyword 2, keyword 3</small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                          Status <a data-toggle="collapse" href="#post-status"><i style="float: right;" class="fa fa-caret-down" aria-hidden="true"></i></a>
                        </h4>
                    </div>
                    <div id="post-status" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <div class="form-group">
                                <select name="status" class="form-control">
                                    <option value="1" {{ $data->status == 1 ? 'selected' : '' }}>Enable</option>
                                    <option value="0" {{ $data->status == 0 ? 'selected' : '' }}>Disable</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                          Keyword Type <a data-toggle="collapse" href="#keyword-type"><i style="float: right;" class="fa fa-caret-down" aria-hidden="true"></i></a>
                        </h4>
                    </div>
                    <div id="keyword-type" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <div class="form-group">
                                <select name="keyword_type" class="form-control select-keywords-type">
                                    <option value="single" {{ $data->keyword_type == 'single' ? 'selected' : '' }}>Single</option>
                                    <option value="global" {{ $data->keyword_type == 'global' ? 'selected' : '' }}>Global</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>


    </form>

</div>
@stop
