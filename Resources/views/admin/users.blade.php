@extends('blog::layouts.master')

@section('content')

<div class="col-md-12">
<a href="{{ route('panel.users__add') }}" class="btn btn-round btn-fill btn-info">New User +<div class="ripple-container"></div></a>
<div class="card">
    <div class="card-header" data-background-color="green">
        <h4 class="title">Users</h4>
        <p class="category">All Users</p>
    </div>

    <div class="card-content table-responsive">
        <table class="table" id="users-table">
            <thead>
                <th>Username</th>
                <th>Email</th>
                <th>Created At</th>
                <th>Action</th>
            </thead>
        </table>
    </div>
</div>
</div>
@stop
