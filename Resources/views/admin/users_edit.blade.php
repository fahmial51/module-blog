@extends('blog::layouts.master')

@section('content')

<div class="col-md-12">
    @if ($errors->any())
    <div class="alert alert-danger alert-dismissable ">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        There is some error. Please check again
    </div>
    @endif

    <form id="post-form" method="post" action="{{ route('panel.users__update', $data->id) }}" accept-charset="UTF-8">
        <div class="row">         
            <div class="col-md-9"> 
                <h4 class="title">Edit User</h4>
            </div>
            <div class="col-md-9 col-sm-6 col-xs-6"> 
                <a href="{{ route('panel.users__add') }}" class="btn btn-round btn-fill btn-info">
                    New User +<div class="ripple-container"></div>
                </a>
                @if(Auth()->user()->id != $data->id)
                <a onclick="return confirm('Delete Page?');" href="{{route('panel.users__delete',$data->id)}}" class="btn btn-round btn-fill btn-danger">
                    Delete User<div class="ripple-container"></div>
                </a>
                @endif
            </div>
            <div class="col-md-3 col-sm-6 col-xs-6">
                <button type="submit" class="btn btn-success pull-right">Save</button>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
            </div>
        </div>
        
        <div class="row" style="margin-top: 15px;">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">Name</label>
                    @if ($errors->has('name'))
                    <div class="has-error">
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    </div>
                    @endif
                    <input class="form-control" type="text" name="name" value="{{ $data->name }}" placeholder="Name" required="required">
                </div>
                
                <div class="form-group">
                    <label class="control-label">Username</label>
                    @if ($errors->has('username'))
                    <div class="has-error">
                        <span class="help-block">
                            <strong>{{ $errors->first('username') }}</strong>
                        </span>
                    </div>
                    @endif
                    <input class="form-control" type="text" name="username" value="{{ $data->username }}" placeholder="Username" required readonly>
                </div>

                <div class="form-group">
                    <label class="control-label">Email</label>
                    @if ($errors->has('email'))
                    <div class="has-error">
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    </div>
                    @endif
                    <input class="form-control" type="email" name="email" value="{{ $data->email }}" placeholder="Email" required="required">
                </div>

                <div class="form-group">
                    <label for="change_password">Edit Password</label> 
                    <label class="switch">
                      <input type="checkbox" name="change_password" value="1" id="change_password" class="form-control">
                      <span class="slider round"></span>
                    </label>
                </div>

                <div class="form-group edit-password" style="display: none;">
                    <label class="control-label">Password</label>
                    @if ($errors->has('password'))
                    <div class="has-error">
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    </div>
                    @endif
                    <input class="form-control" type="password" name="password" value="" placeholder="Password">
                </div>

                <div class="form-group edit-password" style="display: none;">
                    <label class="control-label">Password Confirmation</label>
                    @if ($errors->has('password_confirmation'))
                    <div class="has-error">
                        <span class="help-block">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                    </div>
                    @endif
                    <input class="form-control" type="password" name="password_confirmation" value="" placeholder="Password Confirmaation">
                </div>

                @if(Auth()->user()->id != $data->id || Auth()->user()->role == 'admin')
                <div class="form-group">
                    <label for="role">Select User Role</label>
                    @if ($errors->has('role'))
                    <div class="has-error">
                        <span class="help-block">
                            <strong>{{ $errors->first('role') }}</strong>
                        </span>
                    </div>
                    @endif
                    <select name="role" class="form-control" id="role" required>
                        <option value="admin">Admin</option>
                        <option value="editor">Editor</option>
                    </select>
                </div>
                @endif
            </div>
        </div>


    </form>

</div>
@stop