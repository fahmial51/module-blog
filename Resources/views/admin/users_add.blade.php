@extends('blog::layouts.master')

@section('content')

<div class="col-md-12">
    @if ($errors->any())
    <div class="alert alert-danger alert-dismissable ">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        There is some error. Please check again
    </div>
    @endif

    <form id="post-form" method="post" action="{{ route('panel.users__save') }}" accept-charset="UTF-8">
        <div class="row">         
            <div class="col-md-9"> 
                <h4 class="title">New User</h4>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-6">
                <button type="submit" class="btn btn-success pull-right">Save</button>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
            </div>
        </div>
        
        <div class="row" style="margin-top: 15px;">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">Name</label>
                    @if ($errors->has('name'))
                    <div class="has-error">
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    </div>
                    @endif
                    <input class="form-control" type="text" name="name" value="{{ old('name') }}" placeholder="Name" required="required">
                </div>
                
                <div class="form-group">
                    <label class="control-label">Username</label>
                    @if ($errors->has('username'))
                    <div class="has-error">
                        <span class="help-block">
                            <strong>{{ $errors->first('username') }}</strong>
                        </span>
                    </div>
                    @endif
                    <input class="form-control" type="text" name="username" value="{{ old('username') }}" placeholder="Username" required="required">
                </div>

                <div class="form-group">
                    <label class="control-label">Email</label>
                    @if ($errors->has('email'))
                    <div class="has-error">
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    </div>
                    @endif
                    <input class="form-control" type="email" name="email" value="{{ old('email') }}" placeholder="Email" required="required">
                </div>

                <div class="form-group">
                    <label class="control-label">Password</label>
                    @if ($errors->has('password'))
                    <div class="has-error">
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    </div>
                    @endif
                    <input class="form-control" type="password" name="password" value="" placeholder="Password" required="required">
                </div>

                <div class="form-group">
                    <label class="control-label">Password Confirmation</label>
                    @if ($errors->has('password_confirmation'))
                    <div class="has-error">
                        <span class="help-block">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                    </div>
                    @endif
                    <input class="form-control" type="password" name="password_confirmation" value="" placeholder="Password Confirmaation" required="required">
                </div>

                <div class="form-group">
                    <label for="role">Select User Role</label>
                    @if ($errors->has('role'))
                    <div class="has-error">
                        <span class="help-block">
                            <strong>{{ $errors->first('role') }}</strong>
                        </span>
                    </div>
                    @endif
                    <select name="role" class="form-control" id="role" required>
                        <option value="admin">Admin</option>
                        <option value="editor">Editor</option>
                    </select>
                </div>
            </div>
        </div>


    </form>

</div>
@stop