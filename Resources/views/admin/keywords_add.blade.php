@extends('blog::layouts.master')

@section('content')

<div class="col-md-12">
    @if ($errors->any())
    <div class="alert alert-danger alert-dismissable ">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        There is some error. Please check again
        {{$errors->first()}}
    </div>
    @endif

    <form id="keyword-form" method="post" action="{{ route('panel.keywords__save') }}" accept-charset="UTF-8">
        <div class="row">         
            <div class="col-md-9"> 
                <h4 class="title">New Keyword</h4>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-6">
                <button type="submit" class="btn btn-success pull-right">Save Keyword</button>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
            </div>
        </div>
        
        <div class="row" style="margin-top: 15px;">
            <div class="col-md-9">
                <div class="form-group">
                    <label class="control-label">Title</label>
                    @if ($errors->has('title'))
                    <div class="has-error">
                        <span class="help-block">
                            <strong>{{ $errors->first('title') }}</strong>
                        </span>
                    </div>
                    @endif
                    <input class="form-control post-title" type="text" name="title" value="{{ old('title') }}" placeholder="Enter Title Here" required="required">
                </div>

                <div class="form-group">
                    <label class="control-label">Slug</label>
                    @if ($errors->has('slug'))
                    <div class="has-error">
                        <span class="help-block">
                            <strong>{{ $errors->first('slug') }}</strong>
                        </span>
                    </div>
                    @endif
                    <input class="form-control post-slug" type="text" name="slug" value="{{ old('slug') }}" placeholder="keyword-slug" required="required">
                </div>

                <div class="panel panel-default" id="global-keyword" style="display: none;">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                          Content <a data-toggle="collapse" href="#keyword-content"><i style="float: right;" class="fa fa-caret-down" aria-hidden="true"></i></a>
                        </h4>
                    </div>
                    <div id="keyword-content" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <div class="form-group">
                                <label for="keyword_data_check">Use Page</label> 
                                <label class="switch">
                                  <input type="checkbox" name="data_check" value="1" id="keyword_data_check" class="form-control">
                                  <span class="slider round"></span>
                                </label>
                            </div>

                            <div class="form-group keyword_data_page" style="display: none;">
                                <label for="data_page">Page</label>
                                <select class="myselect2 form-control" name="data_page" id="data_page">
                                    <option value="">Select page</option>
                                    @foreach($pages as $page)
                                    <option value="{{$page->id}}">{{$page->title}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group keyword_data_other">
                                <label for="data_categories">Category</label>
                                <select class="myselect2 form-control" name="data_categories[]" id="data_categories" multiple>
                                    <option value="">Select Category</option>
                                    @foreach($categories as $category)
                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group keyword_data_other">
                                <label for="data_tags">Tag</label>
                                <select class="myselect2 form-control" name="data_tags[]" id="data_tags" multiple>
                                    <option value="">Select Tag</option>
                                    @foreach($tags as $tag)
                                    <option value="{{$tag->id}}">{{$tag->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
               
               
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                          SEO Setting <a data-toggle="collapse" href="#post-seo"><i style="float: right;" class="fa fa-caret-down" aria-hidden="true"></i></a>
                        </h4>
                    </div>
                    <div id="post-seo" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <div class="form-group">
                                <label class="control-label">Meta Title</label>
                                <input value="{{ old('meta_title') }}" class="form-control" type="text" name="meta_title" maxlength="191">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Meta Deskripsi</label>
                                <textarea class="form-control" id="inputan" name="meta_desc" style="min-height: 100px;" maxlength="300">{{ old('meta_desc') }}</textarea>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Keyword</label>
                                <input value="{{ old('meta_keyword') }}" class="form-control" type="text" name="meta_keyword" maxlength="191">
                                <small>Contoh : keyword 1, keyword 2, keyword 3</small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                          Status <a data-toggle="collapse" href="#post-status"><i style="float: right;" class="fa fa-caret-down" aria-hidden="true"></i></a>
                        </h4>
                    </div>
                    <div id="post-status" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <div class="form-group">
                                <select name="status" class="form-control">
                                    <option value="1" selected="selected">Enable</option>
                                    <option value="0" >Disable</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                          Keyword Type <a data-toggle="collapse" href="#keyword-type"><i style="float: right;" class="fa fa-caret-down" aria-hidden="true"></i></a>
                        </h4>
                    </div>
                    <div id="keyword-type" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <div class="form-group">
                                <select name="keyword_type" class="form-control select-keywords-type">
                                    <option value="single" selected="selected">Single</option>
                                    <option value="global" >Global</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>


    </form>

</div>
@stop
